<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>admin/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                <li><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
                <li class="active orange"><a href="<?php echo base_url(); ?>admin/control_technician">Control Technician</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <form class="col s12 m12 l12" action="<?php echo base_url(); ?>admin/search_technician" method="post" accept-charset="utf-8">
        <div class="input-field col s6 m6 l6">
          <i class="material-icons prefix">search</i>
          <input id="search_technician" type="text" name="search_technician" validate="">
          <label for="search_technician">Search</label>
        </div>
        <div class="input-field col s3 m3 l3">
          <select class="" name="filter">
            <option value="" disabled selected>Filter Search by</option>
            <option value="NIK">NIK</option>
            <option value="Name">Name</option>
            <option value="Status">Status</option>
          </select>
        </div>
        <div class="input-field col s3 m3 l3">
          <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
      </form>
    </div>
    <div class="row">
      <a class="waves-effect waves-light teal btn" href="<?php echo base_url(); ?>admin/add_tech">Add Technician</a>
      <a class="waves-effect waves-light teal btn" href="<?php echo base_url(); ?>admin/control_technician">Random</a>
    </div>
    <?php if ($isian > 0): ?>
      <div class="row">
        <div class="col s12 m12 l12">
          <table class="highlight bordered responsive-table">
            <thead>
              <th>No</th>
              <th>NIK</th>
              <th>Name Technician</th>
              <th>Status Technician</th>
              <th>Action</th>
            </thead>
            <tbody>
              <?php
              $i = 1;
              foreach ($josy as $ntaps): ?>
              <tr>
                <td class="hidden"><?php echo $ntaps['id_technician']; ?></td>
                <td><?php echo $i++; ?></td>
                <td><?php echo $ntaps['nik']; ?></td>
                <td><?php echo $ntaps['name_technician']; ?></td>
                <td><?php echo $ntaps['status']; ?></td>
                <td>
                  <a class="dropdown-button btn" data-activates="<?php echo $ntaps['id_technician']; ?>">Action</a>
                  <ul id="<?php echo $ntaps['id_technician']; ?>" class="dropdown-content">
                    <li><a href="edit_tech/<?php echo $ntaps['id_technician']; ?>">Edit</a></li>
                    <li><a href="delete_tech/<?php echo $ntaps['id_technician']; ?>">Delete</a></li>
                  </ul>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    <?php else: ?>
      <div class="row">
        <div class="col s12 m12 l12">
          <p>Data tidak ada</p>
        </div>
      </div>
    <?php endif; ?>
  </div>
</main>
