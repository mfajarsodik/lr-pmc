<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                <li class="active orange"><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <?php foreach ($isi as $isinya) : ?>
  <div class="container section">
    <form class="white z-depth-2" action="<?php echo base_url() ?>admin/act_edit_wo/<?php echo $isinya['id_project']; ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Form Edit WO Project</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input disabled type="text" name="id_project" value="<?php echo $isinya['id_project']; ?>">
              <label for="id_project">ID Project</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field col s12 m4 l4">
              <select class="" name="tanggal">
                <option value="<?php echo $isinya['tanggal']; ?>" selected><?php echo $isinya['tanggal']; ?></option>
                <?php for ($i=1; $i < 32; $i++) { ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <!-- <option value="<?php echo $isinya['tanggal']; ?>" selected><?php echo $isinya['tanggal']; ?></option> -->
                <?php } ?>
              </select>
            </div>
            <div class="input-field col s12 m4 l4">
              <select class="" name="bulan">
                <option value="<?php echo $isinya['bulan']; ?>" selected><?php echo $isinya['bulan']; ?></option>
                <option value="Januari">Januari</option>
                <option value="Februari">Februari</option>
                <option value="Maret">Maret</option>
                <option value="April">April</option>
                <option value="Mei">Mei</option>
                <option value="Juni">Juni</option>
                <option value="Juli">Juli</option>
                <option value="Agustus">Agustus</option>
                <option value="September">September</option>
                <option value="Oktober">Oktober</option>
                <option value="November">November</option>
                <option value="Desember">Desember</option>
              </select>
            </div>
            <div class="input-field col s12 m4 l4">
                <select class="" name="tahun">
                  <option value="<?php echo $isinya['tahun']; ?>" selected><?php echo $isinya['tahun']; ?></option>
                  <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="project" value="<?php echo $isinya['project']; ?>" class="validate">
              <label for="project">Project</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="customer" class="validate" value="<?php echo $isinya['customer'] ?>">
              <label for="customer">Customer</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="id_wo" value="<?php echo $isinya['id_wo']; ?>" class="validate">
              <label for="id_wo">ID WO</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="so" value="<?php echo $isinya['so']; ?>" class="validate">
              <label for="so">SO</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="pic_indosat" class="validate" value="<?php echo $isinya['pic_indosat']; ?>">
              <label for="pic_indosat">PIC Indosat</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="location" class="validate" value="<?php echo $isinya['location']; ?>">
              <label for="location">Location</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="cid" class="validate" value="<?php echo $isinya['cid']; ?>">
              <label for="cid">CID</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <select class="" name="information">
              <option value="<?php echo $isinya['information']; ?>" selected><?php echo $isinya['information']; ?></option>
              <option value="Open">Open</option>
              <option value="Closed">Closed</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <select class="" name="progress">
              <option value="<?php echo $isinya['progress']; ?>" selected><?php echo $isinya['progress']; ?></option>
              <option value="On Progress">On Progress</option>
              <option value="Cancel">Cancel</option>
              <option value="Done">Done</option>
              <option value="Pending">Pending</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit</button>
              <i class="material-icons right">send</i>
          </div>
        </div>
      </div>
    </form>
  </div>
  <?php endforeach; ?>
</main>
