<?php if ($isi > 0) { ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li class="active orange"><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/control_technician">Control Technician</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <form class="white z-depth-2" action="<?php echo base_url(); ?>admin/add_order" method="post" accept-charset="utf-8">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Form Submit Project</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field col s12 m4 l4">
                <select class="" name="tanggal">
                  <option value="" disabled="" selected="">Date</option>
                  <?php for ($i=1; $i < 32; $i++) { ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="input-field col s12 m4 l4">
                <select class="" name="bulan">
                  <option value="" disabled="" selected="">Month</option>
                  <option value="Januari">Januari</option>
                  <option value="Februari">Februari</option>
                  <option value="Maret">Maret</option>
                  <option value="April">April</option>
                  <option value="Mei">Mei</option>
                  <option value="Juni">Juni</option>
                  <option value="Juli">Juli</option>
                  <option value="Agustus">Agustus</option>
                  <option value="September">September</option>
                  <option value="Oktober">Oktober</option>
                  <option value="November">November</option>
                  <option value="Desember">Desember</option>
                </select>
              </div>
              <div class="input-field col s12 m4 l4">
                  <select class="" name="tahun">
                    <option value="" disabled="" selected="">Year</option>
                    <?php
                    $tahun = date("Y");
                    for ($i=2016; $i <= $tahun; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="project"  class="validate" id="project" list="data-project">
                <label for="project">Project</label>
                <datalist id="data-project">
                  <?php
                  $query = $this->db->query("SELECT project FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['project']; ?>"><?php echo $row['project']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="customer" class="validate" id="customer" list="data-customer">
                <label for="customer">Customer</label>
                <datalist id="data-customer">
                  <?php
                  $query = $this->db->query("SELECT customer FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['customer']; ?>"><?php echo $row['customer']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="id_wo" class="validate" list="data-idwo">
                <label for="id_wo">ID WO</label>
                <datalist id="data-idwo">
                  <?php
                  $query = $this->db->query("SELECT id_wo FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['id_wo']; ?>"><?php echo $row['id_wo']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="so" class="validate" list="data-so">
                <label for="so">SO</label>
                <datalist id="data-so">
                  <?php
                  $query = $this->db->query("SELECT so FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['so']; ?>"><?php echo $row['so']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="pic_indosat" class="validate" list="data_pic_indosat">
                <label for="pic_indosat">PIC Indosat</label>
                <datalist id="data_pic_indosat">
                  <?php
                  $query = $this->db->query("SELECT pic_indosat FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['pic_indosat']; ?>"><?php echo $row['pic_indosat']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="location" class="validate" list="data-location">
                <label for="location">Location</label>
                <datalist id="data-location">
                  <?php
                  $query = $this->db->query("SELECT location FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['location']; ?>"><?php echo $row['location']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="cid" class="validate" list="data-cid">
                <label for="cid">CID</label>
                <datalist id="data-cid">
                  <?php
                  $query = $this->db->query("SELECT cid FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['cid']; ?>"><?php echo $row['cid']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </main>
<?php } else { ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li class="active orange"><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/control_technician">Control Technician</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <form class="white z-depth-2" action="<?php echo base_url(); ?>admin/add_order" method="post" accept-charset="utf-8">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Form Submit Project</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field col s12 m4 l4">
                <select class="" name="tanggal">
                  <option value="" disabled="" selected="">Date</option>
                  <?php for ($i=1; $i < 32; $i++) { ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="input-field col s12 m4 l4">
                <select class="" name="bulan">
                  <option value="" disabled="" selected="">Month</option>
                  <option value="Januari">Januari</option>
                  <option value="Februari">Februari</option>
                  <option value="Maret">Maret</option>
                  <option value="April">April</option>
                  <option value="Mei">Mei</option>
                  <option value="Juni">Juni</option>
                  <option value="Juli">Juli</option>
                  <option value="Agustus">Agustus</option>
                  <option value="September">September</option>
                  <option value="Oktober">Oktober</option>
                  <option value="November">November</option>
                  <option value="Desember">Desember</option>
                </select>
              </div>
              <div class="input-field col s12 m4 l4">
                  <select class="" name="tahun">
                    <option value="" disabled="" selected="">Year</option>
                    <?php
                    $tahun = date("Y");
                    for ($i=2016; $i <= $tahun; $i++) { ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="project"  class="validate" list="data-project">
                <label for="project">Project</label>
                <datalist id="data-project">
                  <?php
                  $query = $this->db->query("SELECT project FROM list_project");
                  foreach ($query->result_array() as $row) :
                  ?>
                  <option value="<?php echo $row['project'] ?>"><?php echo $row['project'] ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="customer" class="validate" list="data-customer">
                <label for="customer">Customer</label>
                <datalist id="data-customer">
                  <?php
                  $query = $this->db->query("SELECT customer FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['customer']; ?>"><?php echo $row['customer']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="id_wo" class="validate" list="data-idwo">
                <label for="id_wo">ID WO</label>
                <datalist id="data-idwo">
                  <?php
                  $query = $this->db->query("SELECT id_wo FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['id_wo']; ?>"><?php echo $row['id_wo']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="so" class="validate" list="data-so">
                <label for="so">SO</label>
                <datalist id="data-so">
                  <?php
                  $query = $this->db->query("SELECT so FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['so']; ?>"><?php echo $row['so']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="pic_indosat" class="validate" list="data_pic_indosat">
                <label for="pic_indosat">PIC Indosat</label>
                <datalist id="data_pic_indosat">
                  <?php
                  $query = $this->db->query("SELECT pic_indosat FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['pic_indosat']; ?>"><?php echo $row['pic_indosat']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="location" class="validate" list="data-location">
                <label for="location">Location</label>
                <datalist id="data-location">
                  <?php
                  $query = $this->db->query("SELECT location FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['location']; ?>"><?php echo $row['location']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="cid" class="validate" list="data-cid">
                <label for="cid">CID</label>
                <datalist id="data-cid">
                  <?php
                  $query = $this->db->query("SELECT cid FROM list_project");
                  foreach ($query->result_array() as $row) : ?>
                  <option value="<?php echo $row['cid']; ?>"><?php echo $row['cid']; ?></option>
                  <?php endforeach; ?>
                </datalist>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </main>
<?php } ?>
