<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                <li class="active orange"><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal red darken-1 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <?php foreach ($isi as $isinya) : ?>
    <form class="white z-depth-2" action="<?php echo base_url(); ?>admin/add_reason/<?php echo $isinya['id_project']; ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Form Add Reason</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input disabled type="text" name="id_project" value="<?php echo $isinya['id_project']; ?>">
              <label for="id_project">ID Project</label>
            </div>
          <?php endforeach; ?>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <textarea id="reason_information" name="reason_information" class="materialize-textarea"></textarea>
              <label for="reason_information">Reason Project</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</main>
