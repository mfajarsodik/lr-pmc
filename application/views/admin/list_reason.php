<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                <li class="active orange"><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <form class="col s12" action="<?php echo base_url(); ?>admin/search_filter" method="post" accept-charset="utf-8">
        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">search</i>
            <input id="search_wo" type="text" name="search_wo" validate="">
            <label for="search_wo">Search</label>
          </div>
          <div class="input-field col s3">
            <select class="" name="filter">
              <option value="" disabled selected>Filter Search by</option>
              <option value="ID WO">ID WO</option>
              <option value="Project">Project</option>
              <option value="Technician">Technician</option>
              <option value="Date">Date</option>
              <option value="Customer">Customer</option>
              <option value="SO">SO</option>
              <option value="PIC INDOSAT">PIC INDOSAT</option>
              <option value="Location">Location</option>
              <option value="CID">CID</option>
            </select>
          </div>
          <div class="input-field col s3">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>
    <div class="row">
      <a href="<?php echo base_url(); ?>admin/list_onprogress" class="waves-effect waves-light btn blue darken-1">On Progress</a>
      <a href="<?php echo base_url(); ?>admin/list_cancel" class="waves-effect waves-light btn blue darken-1 ">Cancel</a>
      <a href="<?php echo base_url(); ?>admin/list_done" class="waves-effect waves-light btn blue darken-1">Done</a>
      <a href="<?php echo base_url(); ?>admin/list_pending" class="waves-effect waves-light btn blue darken-1">Pending</a>
      <a href="<?php echo base_url(); ?>admin/list_reason" class="waves-effect waves-light btn disabled">Reason</a>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <table class="bordered highlight responsive-table">
          <thead>
            <th>ID</th>
            <th>ID WO</th>
            <th>Project</th>
            <th>Technician</th>
            <th>Date</th>
            <th>Aksi</th>
          </thead>
          <tbody>
            <?php foreach ($isi as $isinya) : ?>
            <tr>
              <td><?php echo $isinya['id_project']; ?></td>
              <td><?php echo $isinya['id_wo']; ?></td>
              <td><?php echo word_limiter($isinya['project'],5); ?></td>
              <td><?php echo $isinya['technician']; ?></td>
              <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
              <td>
                <a class='dropdown-button btn blue darken-1' href='<?php echo $isinya['id_project']; ?>' data-activates='dropdown-<?php echo $isinya['id_project']; ?>'>Aksi</a>
                <ul id='dropdown-<?php echo $isinya['id_project']; ?>' class='dropdown-content'>
                  <li><a href="<?php echo base_url(); ?>admin/detail_reason/<?php echo $isinya['id_project']; ?>">Detail</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/edit_reason/<?php echo $isinya['id_project']; ?>">Edit</a></li>
                </ul>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</main>
