<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                <li class="active orange"><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row white z-depth-2">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Detail Project WO</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <table class="bordered highlight responsive-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($isi as $isinya) : ?>
                <tr>
                  <td>ID Project</td>
                  <td><?php echo $isinya['id_project']; ?></td>
                </tr>
                <tr>
                  <td>Date Project</td>
                  <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                </tr>
                <tr>
                  <td>Project</td>
                  <td><?php echo $isinya['project']; ?></td>
                </tr>
                <tr>
                  <td>Customer</td>
                  <td><?php echo $isinya['customer']; ?></td>
                </tr>
                <tr>
                  <td>ID WO</td>
                  <td><?php echo $isinya['id_wo']; ?></td>
                </tr>
                <tr>
                  <td>SO</td>
                  <td><?php echo $isinya['so']; ?></td>
                </tr>
                <tr>
                  <td>PIC Indosat</td>
                  <td><?php echo $isinya['pic_indosat']; ?></td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td><?php echo $isinya['location']; ?></td>
                </tr>
                <tr>
                  <td>Technician</td>
                  <td><?php echo $isinya['technician']; ?></td>
                </tr>
                <tr>
                  <td>Progress</td>
                  <td><?php echo $isinya['progress']; ?></td>
                </tr>
                <tr>
                  <td>Information</td>
                  <td><?php echo $isinya['information']; ?></td>
                </tr>
                <tr>
                  <td>CID</td>
                  <td><?php echo $isinya['cid']; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>admin/edit_wo/<?php echo $isinya['id_project']; ?>">Edit</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
