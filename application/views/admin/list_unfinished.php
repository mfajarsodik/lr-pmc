<?php if ($isi > 0) : ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                    <li class="active orange"><a href="<?php echo base_url(); ?>admin/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row">
        <div class="col s12 m12 l12">
          <table class="bordered responsive-table highlight">
            <thead>
              <th>ID Project</th>
              <th>ID WO</th>
              <th>Project</th>
              <th>Customer</th>
              <th>Tanggal</th>
              <th>Action</th>
            </thead>
            <tbody>
              <?php foreach ($isian as $isinya) : ?>
              <tr>
                <td><?php echo $isinya['id_project']; ?></td>
                <td><?php echo $isinya['id_wo']; ?></td>
                <td><?php echo $isinya['project']; ?></td>
                <td><?php echo $isinya['customer']; ?></td>
                <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                <td>
                  <a class='dropdown-button btn blue darken-1' href='<?php echo $isinya['id_project']; ?>' data-activates='dropdown-<?php echo $isinya['id_project']; ?>'>Aksi</a>
                  <ul id='dropdown-<?php echo $isinya['id_project']; ?>' class='dropdown-content'>
                    <li><a href="<?php echo base_url(); ?>admin/detail_wo_done/<?php echo $isinya['id_project']; ?>">Detail</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/done_wo/<?php echo $isinya['id_project']; ?>">Report Done</a></li>
                  </ul>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </main>
<?php else : ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>admin">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/graphic">Graphic Project</a></li>
                    <li class="active orange"><a href="<?php echo base_url(); ?>admin/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>admin/order">Order WO</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/manage_wo">Manage WO</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row">
        <div class="col s12 m12 l12">
          <table class="bordered responsive-table highlight">
            <thead>
              <th>ID Project</th>
              <th>ID WO</th>
              <th>Project</th>
              <th>Customer</th>
              <th>Tanggal</th>
              <th>Action</th>
            </thead>
            <tbody>
              <?php foreach ($isian as $isinya) : ?>
              <tr>
                <td><?php echo $isinya['id_project']; ?></td>
                <td><?php echo $isinya['id_wo']; ?></td>
                <td><?php echo $isinya['project']; ?></td>
                <td><?php echo $isinya['customer']; ?></td>
                <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                <td>
                  <a class='dropdown-button btn blue darken-1' href='<?php echo $isinya['id_project']; ?>' data-activates='dropdown-<?php echo $isinya['id_project']; ?>'>Aksi</a>
                  <ul id='dropdown-<?php echo $isinya['id_project']; ?>' class='dropdown-content'>
                    <li><a href="<?php echo base_url(); ?>admin/detail_wo_done/<?php echo $isinya['id_project']; ?>">Detail</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/done_wo/<?php echo $isinya['id_project']; ?>">Report Done</a></li>
                  </ul>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </main>
<?php endif; ?>
