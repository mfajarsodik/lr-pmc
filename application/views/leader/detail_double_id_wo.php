<header>
	<div class="container">
		<a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
	</div>
	<ul id="nav-mobile" class="side-nav fixed">
		<li class="logo"><a href="<?php echo base_url(); ?>leader">
			<object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
				Your browser does not support SVG
			</object>
		</a></li>
		<li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
	</ul>
</header>
<main>
  <div class="container section">
    <div class="row">
      <a href="<?php echo base_url(); ?>leader/double_id_wo" class="waves-effect waves-light btn blue darken-2">Double Id WO</a>
      <a href="<?php echo base_url(); ?>leader/double_so" class="waves-effect waves-light btn blue darken-2">Double SO</a>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <table class="highlight responsive-table bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Id WO</th>
              <th>SO</th>
              <th>Project</th>
              <th>Date</th>
              <th>Technician</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($isi as $isinya) : ?>
              <tr>
                <td><?php echo $isinya['id_wo']; ?></td>
                <td><?php echo $isinya['so']; ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</main>