  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <?php if ($this->session->flashdata('search_wrong')) : ?>
      <div class="row">
        <div class="col s12 m12 l12 red white-text">
          <h5 class="center-align">Please input what you want to search</h5>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata('input_bulan')) : ?>
      <div class="row">
        <div class="col s12 m12 l12 red white-text">
          <h5 class="center-align">Please input month</h5>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata('input_tahun')) : ?>
      <div class="row">
        <div class="col s12 m12 l12 red white-text">
          <h5 class="center-align">Please input year</h5>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <ul id="tabs-swipe-demo" class="tabs">
    <li class="tab col s6"><a class="active" href="#test-swipe-1">List Scheduled</a></li>
    <li class="tab col s6"><a href="#test-swipe-2">List Unscheduled</a></li>
  </ul>
  <div id="test-swipe-1" class="col s12">
    <div class="row">
      <div class="col s12">
        <form class="col s12 m12 l12" action="<?php echo base_url(); ?>leader/search_scheduled" method="post" accept-charset="utf-8">
          <div class="row">
            <div class="input-field col s6">
              <i class="material-icons prefix">search</i>
              <input id="search_wo" type="text" name="search_wo" validate="">
              <label for="search_wo">Search</label>
            </div>
            <div class="input-field col s3">
              <select class="" name="filter">
                <option value="" disabled selected>Filter Search by</option>
                <option value="ID WO">ID WO</option>
                <option value="Project">Project</option>
                <option value="Date">Date</option>
                <option value="Customer">Customer</option>
                <option value="PIC Indosat">PIC Indosat</option>
                <option value="Technician">Technician</option>
              </select>
            </div>
            <div class="input-field col s3">
              <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
      <div class="col s12 m12 l12">
        <table class="highlight responsive-table bordered">
          <thead>
            <tr>
              <th>ID Project</th>
              <th>ID WO</th>
              <th>Project</th>
              <th>Date</th>
              <th>Action</th>
              <?php
              // TODO: disini muncul schedule inti
              // TODO: ada menu dropdown isinya detail schedule atau tambah schedule baru
              // TODO: buat button untuk extract data scheduling ke excel
              ?>
            </tr>
            <tbody>
              <?php foreach ($isi as $jos) : ?>
                <tr>
                  <td><?php echo $jos['id_project']; ?></td>
                  <td><?php echo $jos['id_wo']; ?></td>
                  <td><?php echo word_limiter($jos['project'],3) ; ?></td>
                  <td><?php echo $jos['tanggal']; ?>-<?php echo $jos['bulan']; ?>-<?php echo $jos['tahun']; ?></td>
                  <td><a class="waves-effect waves-light btn" href="<?php echo site_url('/leader/view_schedule_order_wo/' . $jos['id_project']); ?>">Detail</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </thead>
        </table>
      </div>
  </div>
  <div id="test-swipe-2" class="col s12">
    <div class="row">
      <div class="col s12">
        <form class="col s12 m12 l12" action="<?php echo base_url(); ?>leader/search_unscheduled" method="post" accept-charset="utf-8">
          <div class="row">
            <div class="input-field col s6">
              <i class="material-icons prefix">search</i>
              <input id="search_wo" type="text" name="search_wo" validate="">
              <label for="search_wo">Search</label>
            </div>
            <div class="input-field col s3">
              <select class="" name="filter">
                <option value="" disabled selected>Filter Search by</option>
                <option value="ID WO">ID WO</option>
                <option value="Project">Project</option>
                <option value="Date">Date</option>
                <option value="Customer">Customer</option>
                <option value="PIC Indosat">PIC Indosat</option>
                <option value="Technician">Technician</option>
              </select>
            </div>
            <div class="input-field col s3">
              <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
      <div class="col s12 m12 l12">
        <table class="highlight responsive-table bordered">
          <thead>
            <th>ID Project</th>
            <th>ID WO</th>
            <th>Project</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Action</th>
          </thead>
          <tbody>
            <?php foreach ($isinya as $isian) : ?>
            <tr>
              <td><?php echo $isian['id_project']; ?></td>
              <td><?php echo $isian['id_wo']; ?></td>
              <td><?php echo $isian['project']; ?></td>
              <td><?php echo $isian['tanggal']; ?>-<?php echo $isian['bulan']; ?>-<?php echo $isian['tahun']; ?></td>
              <td><?php echo $isian['technician']; ?></td>
              <td><a class="waves-effect waves-light btn" href="<?php echo site_url('/leader/schedule_order_wo/' . $isian['id_project']); ?>">Schedule Project</a></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>
  </div>
      </div>
    </div>
  </main>
