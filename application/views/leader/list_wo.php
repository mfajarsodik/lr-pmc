<header>
	<div class="container">
		<a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
	</div>
	    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
	<div class="container section">
		<div class="row">
			<a href="<?php echo base_url(); ?>leader/double_id_wo" class="waves-effect waves-light btn blue darken-2">Double Id WO</a>
			<a href="<?php echo base_url(); ?>leader/double_so" class="waves-effect waves-light btn blue darken-2">Double SO</a>
		</div>
		<div class="row">
			<div class="col s12 m12 l12">
				<table class="highlight responsive-table bordered">
					<thead>
						<tr>
							<th>NO</th>
							<th>ID WO</th>
							<th>SO</th>
							<th>Project</th>
							<th>Date</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = $this->uri->segment(3) + 1;
						foreach ($isi as $isinya) : ?>
							<tr>
								<td class="hidden"><?php echo $isinya['id_project']; ?></td>
								<td><?php echo $i++; ?></td>
								<td><?php echo $isinya['id_wo']; ?></td>
								<td><?php echo $isinya['so']; ?></td>
								<td><?php echo $isinya['project']; ?></td>
								<td><?php echo $isinya['tanggal'] ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
								<td>
									<a class="dropdown-button btn" data-activates="<?php echo $isinya['id_project'] ?>">Action</a>
										<ul id="<?php echo $isinya['id_project']; ?>" class="dropdown-content">
											<?php if ($isinya['id_wo'] !=''): ?>
												<li><a href="<?php echo base_url(); ?>leader/detail_with_id_wo/<?php echo $isinya['id_wo']; ?>">Detail By ID WO</a></li>
											<?php endif ?>
											<?php if ($isinya['so'] !=''): ?>
												<li><a href="<?php echo base_url(); ?>leader/detail_with_so/<?php echo $isinya['so']; ?>">Detail By SO</a></li>
											<?php endif ?>
										</ul>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
</main>