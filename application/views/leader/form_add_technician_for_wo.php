<?php foreach ($isi as $isinya) : ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/another_task">Another Task</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
<main>
  <div class="container section">
    <?php echo validation_errors(); ?>
    <form class="white z-depth-2" action="<?php echo base_url(); ?>leader/act_add_technician_for_order_wo/<?php echo $isinya['id_project']; ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Form Continue WO Project</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input disabled type="text" id="disabled" name="id_project" value="<?php echo $isinya['id_project']; ?>">
              <label for="id_project">ID Project</label>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <select multiple class="" name="technician[]">
                <option value="" disabled="" selected="">Choose Technician</option>
                <?php foreach ($teknisi as $nama_teknisi) : ?>
                  <option value="<?php echo $nama_teknisi['name_technician']; ?>"><?php echo $nama_teknisi['name_technician']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</main>
