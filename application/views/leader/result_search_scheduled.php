<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="row">
    <div class="container section">
      <div class="row">
        <a class="waves-light waves-effect btn red" href="<?php echo base_url(); ?>leader/scheduling_wo"><i class="material-icons left">arrow_back_ios</i>Back</a>
      </div>
      <div class="row">
        <div class="col s12">
          <form class="col s12 m12 l12" action="<?php echo base_url(); ?>leader/search_scheduled" method="post" accept-charset="utf-8">
            <div class="row">
              <div class="input-field col s6">
                <i class="material-icons prefix">search</i>
                <input id="search_wo" type="text" name="search_wo" validate="">
                <label for="search_wo">Search</label>
              </div>
              <div class="input-field col s3">
                <select class="" name="filter">
                  <option value="" disabled selected>Filter Search by</option>
                  <option value="ID WO">ID WO</option>
                  <option value="Project">Project</option>
                  <option value="Date">Date</option>
                  <option value="Customer">Customer</option>
                  <option value="PIC Indosat">PIC Indosat</option>
                  <option value="Technician">Technician</option>
                </select>
              </div>
              <div class="input-field col s3">
                <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <?php if ($isian > 0): ?>
        <div class="col s12 m12 l12">
          <table class="highlight responsive-table bordered">
            <thead>
              <th>ID Project</th>
              <th>ID WO</th>
              <th>Date</th>
              <th>Action</th>
            </thead>
            <tbody>
              <?php foreach ($josy as $isinya) : ?>
              <tr>
                <td><?php echo $isinya['id_project']; ?></td>
                <td><?php echo $isinya['id_wo']; ?></td>
                <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                <td><a href="<?php echo base_url(); ?>leader/detail_schedule/<?php echo $isinya['id_scheduling']; ?>" class="waves-effect waves-light btn">Detail</a></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      <?php else: ?>
        <div class="col s12 m12 l12">
          <p>Data Tidak Ada</p>
        </div>
      <?php endif; ?>
    </div>
  </div>
</main>
