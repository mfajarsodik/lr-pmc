<?php if ($tanggal == "NULL") : ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li class="active orange"><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row white z-depth-2">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Notification</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                  <a href="<?php echo base_url(); ?>leader/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello Leader, Admin Is Working On</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
<?php else : ?>
  <?php
  $tglProject = new DateTime($tanggal);
  $now = new DateTime("now");
  if ($tglProject->diff($now) >= "2 days") : ?>

  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>

              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li class="active orange"><a href="<?php echo base_url(); ?>leader/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row white z-depth-2">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Notification</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                  <a href="<?php echo base_url(); ?>leader/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello Leader, Tell Admin About</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <?php else : ?>
    <header>
      <div class="container">
        <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
      </div>
      <ul id="nav-mobile" class="side-nav fixed">
        <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
          <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
            Your browser does not support SVG
          </object>
        </a></li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion pad-20px">
              <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>

                <div class="collapsible-body">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                    <li class="active orange"><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                  </ul>
                </div>
              </li>
            </ul>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
              <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                    <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                    <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                    <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                  </ul>
                </div>
              </li>
            </ul>
        </li>
        <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
    </header>
    <main>
      <div class="container section">
        <div class="row white z-depth-2">
          <div class="submit-project">
            <div class="row">
              <div class="container">
                <h5>Notification</h5>
              </div>
            </div>
            <div class="row">
              <div class="container">
                <div class="collection">
                    <a href="<?php echo base_url(); ?>leader/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello Leader, Admin Is Working On</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  <?php endif; ?>
<?php endif; ?>
