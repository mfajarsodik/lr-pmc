<?php if (count($isi) > 0) { ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <?php if ($this->session->flashdata('search_wrong')) : ?>
      <div class="row">
        <div class="col s12 m12 l12 red white-text">
          <h5 class="center-align">Please input what you want to search</h5>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata('input_bulan')) : ?>
      <div class="row">
        <div class="col s12 m12 l12 red white-text">
          <h5 class="center-align">Please input month</h5>
        </div>
      </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata('input_tahun')) : ?>
      <div class="row">
        <div class="col s12 m12 l12 red white-text">
          <h5 class="center-align">Please input year</h5>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <form class="col s12" action="<?php echo base_url(); ?>leader/search_continue" method="post" accept-charset="utf-8">
          <div class="row">
            <div class="input-field col s6">
              <i class="material-icons prefix">search</i>
              <input id="search_wo" type="text" name="search_wo" validate="">
              <label for="search_wo">Search</label>
            </div>
            <div class="input-field col s3">
              <select class="" name="filter">
                <option value="" disabled selected>Filter Search by</option>
                <option value="ID WO">ID WO</option>
                <option value="Project">Project</option>
                <option value="Date">Date</option>
                <option value="Customer">Customer</option>
              </select>
            </div>
            <div class="input-field col s3">
              <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
      <div class="row">
        <div class="col s12 m12 l12">
          <table class="highlight responsive-table bordered">
            <thead>
              <tr>
                <th>NO</th>
                <th>ID Wo</th>
                <th>Tanggal Project</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i = $this->uri->segment(3) + 1;
              foreach ($isi as $isinya) : ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td class="hidden"><?php echo $isinya['id_project']; ?></td>
                <td><?php echo $isinya['id_wo']; ?></td>
                <td><?php echo $isinya['tanggal']; ?> <?php echo $isinya['bulan']; ?> <?php echo $isinya['tahun']; ?></td>
                <td>
                  <a class='dropdown-button btn blue darken-1' href='<?php echo $isinya['id_project']; ?>' data-activates='dropdown-<?php echo $isinya['id_project']; ?>'>Aksi</a>
                    <ul id='dropdown-<?php echo $isinya['id_project']; ?>' class='dropdown-content'>
                      <li><a href="<?php echo base_url(); ?>leader/detail_order/<?php echo $isinya['id_project']; ?>">Detail</a></li>
                      <li><a href="<?php echo base_url(); ?>leader/add_technician_for_order_wo/<?php echo $isinya['id_project']; ?>">Continue</a></li>
                      <li><a href="<?php echo base_url(); ?>leader/reschedule_order_wo/<?php echo $isinya['id_project']; ?>">Reschedule</a></li>
                    </ul>
                </td>
                <!-- <td><a class="waves-effect waves-light btn" href="<?php echo site_url('/leader/reschedule_order_wo/' . $isinya['id_project']); ?>">Reschedule</a></td> -->
                <!-- <td><a class="waves-effect waves-light btn" href="<?php echo site_url('/leader/add_technician_for_order_wo/' . $isinya['id_project']); ?>">Continue</a></td> -->
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <?php echo $this->pagination->create_links(); ?>
        </div>
      </div>
    </div>
  </main>
<?php } else {?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                  <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row">
        <div class="col s12 m12 l12">
          <p>Data Tidak ada</p>
        </div>
      </div>
    </div>
  </main>
<?php } ?>
