<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                <li><a href="<?php echo base_url(); ?>leader/another_task">Another Task</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
  <div class="container section">
    <main>
    <form class="white z-depth-2" action="<?php echo base_url(); ?>leader/act_schedule_order/<?php echo $isi; ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <a class="waves-effect waves-light btn red" onclick="goBack()"><i class="material-icons left">arrow_back_ios</i>Back</a>
          </div>
        </div>
        <script>
        function goBack()
        {
          window.history.back();
        }
        </script>
        <div class="row">
          <div class="container">
            <h5>Form Scheduling WO Project</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input disabled type="text" id="disabled" name="id_project" value="<?php echo $isi; ?>">
              <label for="id_project">ID Project</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field col s12 m4 l4">
              <select class="" name="tanggal">
                <option value="" disabled="" selected="">Date</option>
                <?php for ($i=1; $i < 32; $i++) { ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="input-field col s12 m4 l4">
              <select class="" name="bulan">
                <option value="" disabled="" selected="">Month</option>
                <option value="Januari">Januari</option>
                <option value="Februari">Februari</option>
                <option value="Maret">Maret</option>
                <option value="April">April</option>
                <option value="Mei">Mei</option>
                <option value="Juni">Juni</option>
                <option value="Juli">Juli</option>
                <option value="Agustus">Agustus</option>
                <option value="September">September</option>
                <option value="Oktober">Oktober</option>
                <option value="November">November</option>
                <option value="Desember">Desember</option>
              </select>
            </div>
            <div class="input-field col s12 m4 l4">
                <select class="" name="tahun">
                  <option value="" disabled="" selected="">Year</option>
                  <?php
                  $tahun = date("Y");
                  for ($i=2016; $i <= $tahun; $i++) { ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="project"  class="validate" id="project" list="data-project">
              <label for="project">Project</label>
              <datalist id="data-project">
                <?php
                $query = $this->db->query("SELECT project FROM list_project");
                foreach ($query->result_array() as $row) : ?>
                <option value="<?php echo $row['project']; ?>"><?php echo $row['project']; ?></option>
                <?php endforeach; ?>
              </datalist>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="customer" class="validate" id="customer" list="data-customer">
              <label for="customer">Customer</label>
              <datalist id="data-customer">
                <?php
                $query = $this->db->query("SELECT customer FROM list_project");
                foreach ($query->result_array() as $row) : ?>
                <option value="<?php echo $row['customer']; ?>"><?php echo $row['customer']; ?></option>
                <?php endforeach; ?>
              </datalist>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="id_wo" class="validate" list="data-idwo">
              <label for="id_wo">ID WO</label>
              <datalist id="data-idwo">
                <?php
                $query = $this->db->query("SELECT id_wo FROM list_project");
                foreach ($query->result_array() as $row) : ?>
                <option value="<?php echo $row['id_wo']; ?>"><?php echo $row['id_wo']; ?></option>
                <?php endforeach; ?>
              </datalist>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="pic_indosat" class="validate" list="data_pic_indosat">
              <label for="pic_indosat">PIC Indosat</label>
              <datalist id="data_pic_indosat">
                <?php
                $query = $this->db->query("SELECT pic_indosat FROM list_project");
                foreach ($query->result_array() as $row) : ?>
                <option value="<?php echo $row['pic_indosat']; ?>"><?php echo $row['pic_indosat']; ?></option>
                <?php endforeach; ?>
              </datalist>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="location" class="validate" list="data-location">
              <label for="location">Location</label>
              <datalist id="data-location">
                <?php
                $query = $this->db->query("SELECT location FROM list_project");
                foreach ($query->result_array() as $row) : ?>
                <option value="<?php echo $row['location']; ?>"><?php echo $row['location']; ?></option>
                <?php endforeach; ?>
              </datalist>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <select multiple class="" name="technician[]">
                <option value="" disabled="" selected="">Choose Technician</option>
                <?php
                $this->db->order_by('name_technician');
                $query = $this->db->get('technician_lr');
                foreach ($query->result_array() as $row) : ?>
                <option value="<?php echo $row['name_technician']; ?>"><?php echo $row['name_technician']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <textarea id="keterangan" name="keterangan" class="materialize-textarea"></textarea>
              <label for="keterangan">Note</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <select class="" name="progress">
                <option value="" disabled="" selected="">Choose Progress</option>
                <option value="On Progress">On Progress</option>
                <option value="Cancel">Cancel</option>
                <option value="Pending">Pending</option>
                <option value="Done">Done</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</main>
