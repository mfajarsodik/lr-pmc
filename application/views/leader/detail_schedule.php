<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>leader">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>leader/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>leader/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>leader/continue_wo">Continue WO</a></li>
                <li><a class="active orange white-text" href="<?php echo base_url(); ?>leader/scheduling_wo">Scheduling WO</a></li>
                <li><a href="<?php echo base_url(); ?>leader/progress_wo">Progress WO</a></li>
                <li><a href="<?php echo base_url(); ?>leader/list_wo">List WO Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>leader/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <div class="submit-project z-depth-2">
        <div class="container">
          <a class="waves-effect waves-light btn red" onclick="goBack()"><i class="material-icons left">arrow_back_ios</i>Back</a>
          <?php foreach ($isi as $isinya) : ?>
          <a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>leader/view_schedule_order_wo/<?php echo $isinya['id_project']; ?>">Main Project</a>
          <table class="bordered highlight responsive-table">
            <thead>
              <th>Name</th>
              <th>Detail</th>
            </thead>
            <tbody>
              <tr>
                <td>ID Project</td>
                <td><?php echo $isinya['id_project']; ?></td>
              </tr>
              <tr>
                <td>ID Scheduling</td>
                <td><?php echo $isinya['id_scheduling']; ?></td>
              </tr>
              <tr>
                <td>Date</td>
                <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
              </tr>
              <tr>
                <td>Project</td>
                <td><?php echo $isinya['project']; ?></td>
              </tr>
              <tr>
                <td>Customer</td>
                <td><?php echo $isinya['customer']; ?></td>
              </tr>
              <tr>
                <td>ID WO</td>
                <td><?php echo $isinya['id_wo']; ?></td>
              </tr>
              <tr>
                <td>PIC INDOSAT</td>
                <td><?php echo $isinya['pic_indosat']; ?></td>
              </tr>
              <tr>
                <td>Location</td>
                <td><?php echo $isinya['location']; ?></td>
              </tr>
              <tr>
                <td>Technician</td>
                <td><?php echo $isinya['technician']; ?></td>
              </tr>
              <tr>
                <td>Note</td>
                <td><?php echo $isinya['keterangan']; ?></td>
              </tr>
              <tr>
                <td>Status</td>
                <td><?php echo $isinya['status']; ?></td>
              </tr>
            </tbody>
          </table>
          <?php endforeach; ?>
      <script>
      function goBack()
      {
        window.history.back();
      }
      </script>
      <div class="row">

      </div>
          <div class="row">
            <a class="waves-effect waves-light btn" href="#">Edit</a>
            <a class="waves-effect waves-light btn" href="#">Delete</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
