<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
  body {
    background: url("<?php echo base_url(); ?>assets/image/image-bg.jpg") no-repeat center center fixed;
    background-size: cover;
  }
  img.col.s4 {
    height: 45%;
    position: relative;
    top: 10px;
  }
</style>
<div class="panel-register">
  <?php echo validation_errors(); ?>
  <div class="register-style">
    <form class="register-form white z-depth-5" action="<?php echo base_url(); ?>menu/login_valid" method="post" accept-charset="utf-8">
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="row">
              <div class="image-login-register">
                <h5 class="col s8">Tracing Project Apps</h5>
                <img class="col s4" src="<?php echo base_url(); ?>assets/image/lrlogo.png" alt="logo">
              </div>
            </div>
            <div class="row">
              <div class="input-field">
                <i class="material-icons prefix">account_circle</i>
                <input id="username" type="text" class="validate" name="username" required autofocus>
                <label for="username">Username</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field">
                <i class="material-icons prefix">vpn_key</i>
                <input id="password" type="password" class="validate" name="password" required>
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <button class="btn waves-effect waves-light login-btn orange" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
