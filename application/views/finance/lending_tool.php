<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>finance">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>finance/graphic">Graphic Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a class="orange white-text" href="<?php echo base_url(); ?>finance/lending_tool">Lending Tools</a></li>
                <li><a href="<?php echo base_url(); ?>finance/operational_funds">Operational Funds</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>finance/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <ul id="tabs-swipe-demo" class="tabs">
        <li class="tab col s6"><a href="#test-swipe-1" class="active">Unadded</a></li>
        <li class="tab col s6"><a href="#test-swipe-2">Added</a></li>
      </ul>
      <div class="col s12" id="test-swipe-1">
        <div class="col s12 m12 l12">
          <table class="col s12 m12 l12 bordered highlight responsive-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>ID WO</th>
                <th>Date</th>
                <th>PIC</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($tidakisi as $ntaps): ?>
                <tr>
                  <td><?php echo $ntaps['id_project']; ?></td>
                  <td><?php echo $ntaps['id_wo']; ?></td>
                  <td><?php echo $ntaps['tanggal']; ?>-<?php echo $ntaps['bulan']; ?>-<?php echo $ntaps['tahun']; ?></td>
                  <td><?php echo $ntaps['pic_indosat']; ?></td>
                  <td><a href="<?php echo base_url(); ?>/finance/add_lending_tool/<?php echo $ntaps['id_project']; ?>" class="waves-effect waves-light btn">Add Lending</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col s12 m12 l12" id="test-swipe-2">
        <div class="col s12 m12 l12">
          <table class="col s12 m12 l12 bordered highlight responsive-table">
            <thead>
              <tr>
                <th>ID</th>
                <th>ID WO</th>
                <th>Date</th>
                <th>PIC</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($isi as $isinya): ?>
                <tr>
                  <td><?php echo $isinya['id_project']; ?></td>
                  <td><?php echo $isinya['id_wo']; ?></td>
                  <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                  <td><?php echo $isinya['pic_indosat']; ?></td>
                  <td><a href="<?php echo base_url(); ?>" class="waves-effect waves-light btn">View</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</main>
