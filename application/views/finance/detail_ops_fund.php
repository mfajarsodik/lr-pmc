<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>finance">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>finance/graphic">Graphic Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>finance/lending_tool">Lending Tools</a></li>
                <li><a class="orange white-text" href="<?php echo base_url(); ?>finance/operational_funds">Operational Funds</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>finance/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row white z-depth-2">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Detail Project WO</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <table class="bordered highlight responsive-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($isi as $isinya): ?>
                  <tr>
                    <td>ID Project</td>
                    <td><?php echo $isinya['id_project']; ?></td>
                  </tr>
                  <tr>
                    <td>ID WO</td>
                    <td><?php echo $isinya['id_wo']; ?></td>
                  </tr>
                  <tr>
                    <td>Date</td>
                    <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                  </tr>
                  <tr>
                    <td>PIC</td>
                    <td><?php echo $isinya['pic_indosat']; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>finance/edit_ops_fund/<?php echo $isinya['id_project']; ?>">Edit Ops Fund</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
