<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>finance">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>finance/graphic">Graphic Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a class="orange white-text" href="<?php echo base_url(); ?>finance/lending_tool">Lending Tools</a></li>
                <li><a href="<?php echo base_url(); ?>finance/operational_funds">Operational Funds</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>finance/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<?php foreach ($isi as $isinya) : ?>
<main>
  <div class="container section">
    <form class="white z-depth-2" action="<?php echo base_url(); ?>finance/act_add_lending/<?php echo $isinya['id_project'] ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Form Add Lending Tools</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input disabled type="text" id="disabled" name="id_project" value="<?php echo $isinya['id_project']; ?>">
              <label for="id_project">ID Project</label>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <select multiple class="" name="lending[]">
                <option value="" disabled="" selected="">Choose Lending</option>
                <option value="Alat">Alat</option>
                <option value="Mobil">Mobil</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <textarea id="lending_detail" name="lending_detail" rows="8" cols="80" class="materialize-textarea"></textarea>
              <label for="lending_detail">Lending Detail</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</main>
