<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>finance">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>finance/graphic">Graphic Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Task</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>finance/lending_tool">Lending Tools</a></li>
                <li><a href="<?php echo base_url(); ?>finance/operational_funds">Operational Funds</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>finance/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section center-content">
    <div class="row">
      <div class="col s12 m12 l12 white z-depth-2">
        <?php
        $id_user = $this->session->userdata('id_user');
        $query = $this->db->query("SELECT * FROM user WHERE id_user = $id_user");
        foreach ($query->result_array() as $jos) {
          $id_usernya = $jos['name'];
        }
         ?>
        <h3 class="center-align">Welcome <?php echo $id_usernya; ?></h3>
        <!-- <?php echo $this->session->userdata('position'); ?> -->
        <p class="center-align">This is the trace project of LR Com</p>
      </div>
    </div>
    <div class="row white z-depth-2">
      <div class="col s12 m6 l6">
        <h3 class="center-align">Additional.</h3>
        <p class="justify-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div class="col s12 m6 l6">
        <h3 class="center-align">Additional.</h3>
        <p class="justify-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </div>
  </div>
</main>
<footer>
  <div class="footer-copyright orange darken-2 white-text center-align">
    <p class="footer-text">&copy; <?php echo date('Y'); ?> PT LUMBUNG RIANG COMMUNICATION</p>
  </div>
</footer>
