<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- inject:js -->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/Chart.min.js" charset="utf-8"></script>


<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url: "<?php echo base_url(); ?>admin/datagraphic",
      method: "GET",
      success: function(data) {
        console.log(data);

        var done = [];
        var progress = [];
        var cancel = [];
        var pending = [];

        done.push(data[0][0]);
        progress.push(data[0][1]);
        cancel.push(data[0][2]);
        pending.push(data[0][3]);

        console.log(done);
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Done", "On Progress", "Pending", "Cancel"],
                datasets: [{
                    label: 'Data Progress',
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    data: [9, 12, 15, 2]
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

        // var chartdata = {
        //   labels: 'All Project',
        //   datasets: [
        //     {
        //       label:'Progress',
        //       backgroundColor: 'rgba(200, 200, 200, 0.75)',
        //       borderColor: 'rgba(200, 200, 200, 0.75)',
        //       hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
        //       hoverBorderColor: 'rgba(200, 200, 200, 1)',
        //       data: done, progress, cancel, pending
        //     }
        //   ]
        // };
        //
        // var ctx = document.getElementById('myChart').getContext('2d');
        //
        // var barGraph = new Chart(ctx, {
        //   type: 'bar',
        //   data: chartdata
        // });
      },
      error: function(data) {
        console.log(data);
      }
    });
  });
</script>
<script src="<?php echo base_url(); ?>assets/js/materialize.js"></script>
<!-- endinject -->
</body>
</html>
