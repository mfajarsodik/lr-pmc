<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!--
_     ______  _____ ________  ___
| |    | ___ \/  __ \  _  |  \/  |
| |    | |_/ /| /  \/ | | | .  . |
| |    |    / | |   | | | | |\/| |
| |____| |\ \ | \__/\ \_/ / |  | |
\_____/\_| \_| \____/\___/\_|  |_/

-->
 <!DOCTYPE html>
 <html>
 <!-- Developed by Desktop, Web and Mobile Apps Developer Team PT Lumbung Riang Communication -->
   <head>
     <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/image/logo-web-lr.png">
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="author" content="Mochamad Fajar Sodik">
     <meta name="copyright" content="&copy;Mochamad Fajar Sodik">
     <meta name="description" content="Apps by Lumbung Riang Developer Team for help your task.">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Pemsi LRComm</title>
     <!-- Add to homescreen for Chrome on Android -->
     <meta name="mobile-web-app-capable" content="yes">
     <!-- Add to homescreen for Safari on iOS -->
     <meta name="apple-mobile-web-app-capable" content="yes">
     <meta name="apple-mobile-web-app-status-bar-style" content="white">
     <meta name="apple-mobile-web-app-title" content="Lumbung Riang Apps">
     <!-- Tile icon for Win8 (144x144 + tile color) -->
     <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
     <meta name="msapplication-TileColor" content="#ffffff">

     <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,100,700,900' rel='stylesheet'
           type='text/css'>
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <!-- inject:css -->
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/materialize.css">
     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lr.css">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
     <!-- endinject -->
   </head>
   <body>
    <?php if ($this->session->flashdata('login_failed')) :?>
      <div class="container">
        <div class="card-panel red darken-2 white-text"><?php echo $this->session->flashdata('login_failed'); ?></div>
      </div>
    <?php endif; ?>
