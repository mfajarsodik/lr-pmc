<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li class="active orange white-text"><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row white z-depth-2">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Notification</h5>
          </div>
        </div>
        <?php if (count($ba) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_no_ba" class="collection-item"><span class="new badge"></span>Input BA</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>

        <?php if (count($boq) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_no_boq" class="collection-item"><span class="new badge"></span>Input BOQ</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>

        <?php if (count($pb) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_no_pb" class="collection-item"><span class="new badge"></span>Input PB</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>

        <?php if (count($po) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_no_po" class="collection-item"><span class="new badge"></span>Input PO</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>

        <?php if (count($no_invoice) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_no_invoice" class="collection-item"><span class="new badge"></span>Input No Invoice</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>

        <?php if (count($paid) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_paid" class="collection-item"><span class="new badge"></span>Input Paid Status</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>

        <?php if (count($tgl_invoice) > 0): ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>collection/list_tgl" class="collection-item"><span class="new badge"></span>Input Date Invoice</a>
              </div>
            </div>
          </div>
        <?php else: ?>

        <?php endif; ?>
      </div>
    </div>
  </div>
</main>
