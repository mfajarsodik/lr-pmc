<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a class="orange white-text" href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <form class="col s12 m12 l12" action="<?php echo base_url(); ?>collection/search_rekap" method="post" accept-charset="utf-8">
        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">search</i>
            <input id="search_rekap" type="text" name="search_rekap" class="validate">
            <label for="search_rekap">Search</label>
          </div>
          <div class="input-field col s3">
            <select class="" name="filter">
              <option value="" disabled="" selected="">Filter Search By</option>
              <option value="no_ba">NO BA</option>
              <option value="deskripsi">Deskripsi</option>
              <option value="pm">PM</option>
            </select>
          </div>
          <div class="input-field col s3">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>
    <div class="row white z-depth-2">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5 class="center-align">Detail Rekap BA</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <hr>
            <table class="bordered highlight responsive-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($isi as $isinya): ?>
                  <tr>
                    <td>NO BA</td>
                    <td><?php echo $isinya['no_ba']; ?></td>
                  </tr>
                  <tr>
                    <td>Deskripsi</td>
                    <td><?php echo $isinya['deskripsi']; ?></td>
                  </tr>
                  <tr>
                    <td>PM</td>
                    <td><?php echo $isinya['pm']; ?></td>
                  </tr>
                  <tr>
                    <td>Nominal</td>
                    <td><?php echo "Rp." . number_format($isinya['nominal'] , 0 , "" , '.') . ",-"; ?></td>
                  </tr>
                  <tr>
                    <td>Verifikasi</td>
                    <td><?php echo $isinya['verifikasi']; ?></td>
                  </tr>
                  <tr>
                    <td>NO PB</td>
                    <td><?php echo $isinya['no_pb']; ?></td>
                  </tr>
                  <tr>
                    <td>PB DATE</td>
                    <td><?php echo $isinya['pb_date']; ?></td>
                  </tr>
                  <tr>
                    <td>SC</td>
                    <td><?php echo $isinya['sc']; ?></td>
                  </tr>
                  <tr>
                    <td>PO NO</td>
                    <td><?php echo $isinya['po_no']; ?></td>
                  </tr>
                  <tr>
                    <td>PO DATE</td>
                    <td><?php echo $isinya['po_date']; ?></td>
                  </tr>
                  <tr>
                    <td>GR</td>
                    <td><?php echo $isinya['gr']; ?></td>
                  </tr>
                  <tr>
                    <td>Invoice No</td>
                    <td><?php echo $isinya['invoice_no']; ?></td>
                  </tr>
                  <tr>
                    <td>Payment Status</td>
                    <td><?php echo $isinya['payment_status']; ?></td>
                  </tr>
                  <tr>
                    <td>Payment Date</td>
                    <td><?php echo $isinya['payment_date']; ?></td>
                  </tr>
                  <tr>
                    <td>Final Status</td>
                    <td><?php echo $isinya['final_status']; ?></td>
                  </tr>
                  <tr>
                    <td>Notes</td>
                    <td><?php echo $isinya['notes']; ?></td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <a href="<?php echo base_url(); ?>collection/edit_rekap/<?php echo $isinya['no_ba']; ?>" class="waves-effect waves-light btn">Edit</a>
            <a href="<?php echo base_url(); ?>collection/delete_rekap/<?php echo $isinya['no_ba']; ?>" class="waves-effect waves-light btn">Delete</a>
          <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
