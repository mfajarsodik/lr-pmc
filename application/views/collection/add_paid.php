<header>
  <div class="container section">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icon">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a class="active orange white-text" href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </ul>
</header>
<main>
  <div class="container section">
    <form class="white z-depth-2" action="<?php echo base_url(); ?>collection/act_add_paid/<?php echo $id_project; ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Form Add Paid Status For Book Of Life</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input disabled type="text" name="id_project" value="<?php echo $id_project; ?>">
              <label for="id_project">ID Project</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <select class="" name="status_paid">
                <option value="" disabled="" selected="">Paid Status</option>
                <option value="Paid">Paid</option>
                <option value="Unpaid">Unpaid</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <a href="<?php echo base_url(); ?>collection/act_add_paid/<?php echo $id_project; ?>" class="waves-effect waves-light btn">Add Paid Status</a>
          </div>
        </div>
      </div>
    </form>
  </div>
</main>
