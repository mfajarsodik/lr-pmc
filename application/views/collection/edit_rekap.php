<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a class="orange white-text" href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <?php foreach ($isi as $isinya): ?>
      <form class="white z-depth-2" action="<?php echo base_url(); ?>collection/act_edit_rekap/<?php echo $isinya['no_ba']; ?>" method="post" accept-charset="utf-8">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Form Edit Rekap BA</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="no_ba" value="<?php echo $isinya['no_ba']; ?>" class="validate" disabled>
                <label for="no_ba">NO BA</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="deskripsi" value="<?php echo $isinya['deskripsi']; ?>" class="validate">
                <label for="deskripsi">Deskripsi</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="pm" value="<?php echo $isinya['pm']; ?>" class="validate">
                <label for="pm">PM</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="number" name="nominal" value="<?php echo $isinya['nominal']; ?>" class="validate">
                <label for="nominal">Nominal</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="verifikasi" value="<?php echo $isinya['verifikasi']; ?>" class="validate">
                <label for="verifikasi">Verifikasi</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="no_pb" value="<?php echo $isinya['no_pb']; ?>" class="validate">
                <label for="no_pb">NO PB</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="pb_date" value="<?php echo $isinya['pb_date']; ?>" class="validate">
                <label for="pb_date">PB DATE</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="sc" value="<?php echo $isinya['sc']; ?>" class="validate">
                <label for="sc">SC</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="po_no" value="<?php echo $isinya['po_no']; ?>" class="validate">
                <label for="po_no">PO NO</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="po_date" value="<?php echo $isinya['po_date']; ?>" class="validate">
                <label for="po_date">PO DATE</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="gr" value="<?php echo $isinya['gr']; ?>" class="validate">
                <label for="gr">GR</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="invoice_no" value="<?php echo $isinya['invoice_no']; ?>" class="validate">
                <label for="invoice_no">Invoice No</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <select class="" name="payment_status">
                  <option value="" disabled="" selected="">Payment Status</option>
                  <option value="Paid">Paid</option>
                  <option value="Unpaid">Unpaid</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="payment_date" value="<?php echo $isinya['payment_date']; ?>" class="validate">
                <label for="payment_date">Payment Date</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="final_status" value="<?php echo $isinya['final_status']; ?>" class="validate">
                <label for="final_status">Final Status</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="input-field">
                <input type="text" name="notes" value="<?php echo $isinya['notes']; ?>" class="validate">
                <label for="notes">Notes</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </div>
      </form>
    <?php endforeach; ?>
  </div>
</main>
