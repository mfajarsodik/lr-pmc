<header>
  <div class="container section">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icon">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a class="active orange white-text" href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </ul>
</header>
<main>
  <div class="container section">
    <form class="white z-depth-2" action="<?php echo base_url(); ?>collection/exp_rekap_ba" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Choose Month And Year</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field col s12 m6 l6">
              <select class="" name="bulan">
                <option value="" disabled="" selected="">Month</option>
                <option value="Januari">Januari</option>
                <option value="Februari">Februari</option>
                <option value="Maret">Maret</option>
                <option value="April">April</option>
                <option value="Mei">Mei</option>
                <option value="Juni">Juni</option>
                <option value="Juli">Juli</option>
                <option value="Agustus">Agustus</option>
                <option value="September">September</option>
                <option value="Oktober">Oktober</option>
                <option value="November">November</option>
                <option value="Desember">Desember</option>
              </select>
            </div>
            <div class="input-field col s12 m4 l4">
                <select class="" name="tahun">
                  <option value="" disabled="" selected="">Year</option>
                  <?php
                  $tahun = date("Y");
                  for ($i=2016; $i <= $tahun; $i++) { ?>
                  <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                  <?php } ?>
                </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</main>
