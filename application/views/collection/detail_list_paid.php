<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a class="active orange white-text" href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row white z-depth-2">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5 class="center-align">Detail Project</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <hr>
            <table class="bordered highlight responsive-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($isi as $isinya): ?>
                  <tr>
                    <td>ID Project</td>
                    <td><?php echo $isinya['id_project']; ?></td>
                  </tr>
                  <tr>
                    <td>Date</td>
                    <td><?php echo $isinya['tanggal']; ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun']; ?></td>
                  </tr>
                  <tr>
                    <td>Project</td>
                    <td><?php echo $isinya['project']; ?></td>
                  </tr>
                  <tr>
                    <td>Customer</td>
                    <td><?php echo $isinya['customer']; ?></td>
                  </tr>
                  <tr>
                    <td>ID WO</td>
                    <td><?php echo $isinya['id_wo']; ?></td>
                  </tr>
                  <tr>
                    <td>SO</td>
                    <td><?php echo $isinya['so']; ?></td>
                  </tr>
                  <tr>
                    <td>PIC INDOSAT</td>
                    <td><?php echo $isinya['pic_indosat']; ?></td>
                  </tr>
                  <tr>
                    <td>Location</td>
                    <td><?php echo $isinya['location']; ?></td>
                  </tr>
                  <tr>
                    <td>Technician</td>
                    <td><?php echo $isinya['technician']; ?></td>
                  </tr>
                  <tr>
                    <td>Information</td>
                    <td><?php echo $isinya['information']; ?></td>
                  </tr>
                  <tr>
                    <td>Progress</td>
                    <td><?php echo $isinya['progress']; ?></td>
                  </tr>
                  <tr>
                    <td>CID</td>
                    <td><?php echo $isinya['cid']; ?></td>
                  </tr>
                  <tr>
                    <td>Date Submit Order</td>
                    <td><?php echo $isinya['created_at']; ?></td>
                  </tr>
                  <tr>
                    <td>Last Updated</td>
                    <td><?php echo $isinya['updated_at']; ?></td>
                  </tr>
                  <?php if ($isinya['finished_at'] == "0000-00-00"): ?>
                    <tr>
                      <td>Is Finished</td>
                      <td>No</td>
                    </tr>
                  <?php else: ?>
                    <tr>
                      <td>Finished At</td>
                      <td><?php echo $isinya['finished_at']; ?></td>
                    </tr>
                  <?php endif; ?>
                  <tr>
                    <td>Price Codefication</td>
                    <td><?php echo $isinya['kodefikasi_harga']; ?></td>
                  </tr>
                  <tr>
                    <td>Prices</td>
                    <td><?php echo $isinya['harga']; ?></td>
                  </tr>
                  <tr>
                    <td>NO BOQ</td>
                    <td><?php echo $isinya['no_boq']; ?></td>
                  </tr>
                  <tr>
                    <td>NO BA</td>
                    <td><?php echo $isinya['no_ba']; ?></td>
                  </tr>
                  <tr>
                    <td>NO PB</td>
                    <td><?php echo $isinya['no_pb']; ?></td>
                  </tr>
                  <tr>
                    <td>NO PO</td>
                    <td><?php echo $isinya['no_po']; ?></td>
                  </tr>
                  <tr>
                    <td>No Invoice</td>
                    <td><?php echo $isinya['no_invoice']; ?></td>
                  </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <a href="<?php echo base_url(); ?>collection/add_paid/<?php echo $isinya['id_project']; ?>" class="waves-effect waves-light btn">Add Paid Status</a>
          </div>
        </div>
      <?php endforeach; ?>
      </div>
    </div>
  </div>
</main>
