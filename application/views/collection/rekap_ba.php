<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
              <li><a class="orange white-text" href="<?php echo base_url(); ?>collection/rekap_ba">Rekap BA</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <form class="col s12 m12 l12" action="<?php echo base_url(); ?>collection/search_rekap" method="post" accept-charset="utf-8">
        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">search</i>
            <input id="search_rekap" type="text" name="search_rekap" class="validate">
            <label for="search_rekap">Search</label>
          </div>
          <div class="input-field col s3">
            <select class="" name="filter">
              <option value="" disabled="" selected="">Filter Search By</option>
              <option value="no_ba">NO BA</option>
              <option value="deskripsi">Deskripsi</option>
              <option value="pm">PM</option>
            </select>
          </div>
          <div class="input-field col s3">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>
    <div class="row">
      <a href="<?php echo base_url(); ?>collection/add_new_rekap" class="waves-effect waves-light btn blue darken-1">Add New Rekap</a>
    </div>
    <?php if (count($isi) > 0): ?>
      <div class="row">
        <div class="col s12 m12 l12">
          <table class="bordered highlight responsive-table">
            <thead>
              <tr>
                <th>No BA</th>
                <th>Deskripsi</th>
                <th>PM</th>
                <th>Final Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($isi as $isinya): ?>
                <tr>
                  <td><?php echo $isinya['no_ba']; ?></td>
                  <td><?php echo $isinya['deskripsi']; ?></td>
                  <td><?php echo $isinya['pm']; ?></td>
                  <td><?php echo $isinya['final_status']; ?></td>
                  <td>
                    <a class='dropdown-button btn blue darken-1' href='<?php echo $isinya['no_ba']; ?>' data-activates='dropdown-<?php echo $isinya['no_ba']; ?>'>Aksi</a>
                    <ul id="dropdown-<?php echo $isinya['no_ba']; ?>" class="dropdown-content">
                      <li><a href="<?php echo base_url(); ?>collection/detail_rekap/<?php echo $isinya['no_ba']; ?>">Detail</a></li>
                      <li><a href="<?php echo base_url(); ?>collection/edit_rekap/<?php echo $isinya['no_ba']; ?>">Edit</a></li>
                      <li><a href="<?php echo base_url(); ?>collection/delete_rekap/<?php echo $isinya['no_ba']; ?>">Delete</a></li>
                    </ul>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
  <?php else: ?>
    <div class="row">
      <p>Data tidak ada</p>
    </div>
  <?php endif; ?>
  </div>
</main>
