<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>collection">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>collection/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>collection/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>collection/export_rekap">Rekap BA</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a class="orange white-text" href="<?php echo base_url(); ?>collection/control_project">Control Project</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>collection/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <form class="col s12 m12 l12" action="<?php echo base_url(); ?>collection/search_project" method="post" accept-charset="utf-8">
        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">search</i>
            <input type="text" name="search_wo" class="validate" id="search_wo">
            <label for="search_wo">Search</label>
          </div>
          <div class="input-field col s3">
            <select class="" name="filter">
              <option value="" disabled="" selected="">Filter Search By</option>
              <option value="ID WO">ID WO</option>
              <option value="SO">SO</option>
              <option value="NO BA">NO BA</option>
              <option value="NO PO">NO PO</option>
              <option value="Invoice">Invoice</option>
            </select>
          </div>
          <div class="input-field col s3">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>
    <div class="row">
      <a href="<?php echo base_url(); ?>collection/list_onprogress" class="waves-effect waves-light btn blue darken-1">On Progress</a>
      <a href="<?php echo base_url(); ?>collection/list_cancel" class="waves-effect waves-light btn blue darken-1">Cancel</a>
      <a href="<?php echo base_url(); ?>collection/list_done" class="waves-effect waves-light btn blue darken-1">Done</a>
      <a href="<?php echo base_url(); ?>collection/list_pending" class="waves-effect waves-light btn blue darken-1">Pending</a>
      <a href="<?php echo base_url(); ?>collection/list_reason" class="waves-effect waves-light btn disabled">Reason</a>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <table class="bordered highlight responsive-table">
          <thead>
            <th>ID</th>
            <th>ID WO</th>
            <th>Date</th>
            <th>Project</th>
            <th>Status</th>
            <th>Action</th>
          </thead>
        </table>
      </div>
    </div>
  </div>
</main>
