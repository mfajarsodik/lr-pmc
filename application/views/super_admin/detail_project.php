<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>super_admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/tech_log">Technician Log</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>super_admin/control_technician">Control Technician</a></li>
              <li><a href="<?php echo base_url(); ?>super_admin/control_user">Control User</a></li>
              <li><a class="active orange white-text" href="<?php echo base_url(); ?>super_admin/control_project">Control Project</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>super_admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row white z-depth-2">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5 class="center-align">Detail Project</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <hr>
            <table class="bordered highlight responsive-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($isi as $isinya) : ?>
                <tr>
                  <td>ID Project</td>
                  <td><?php echo $isinya['id_project']; ?></td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td><?php echo $isinya['tanggal'] ?>-<?php echo $isinya['bulan']; ?>-<?php echo $isinya['tahun'] ?></td>
                </tr>
                <tr>
                  <td>Project</td>
                  <td><?php echo $isinya['project']; ?></td>
                </tr>
                <tr>
                  <td>Customer</td>
                  <td><?php echo $isinya['customer']; ?></td>
                </tr>
                <tr>
                  <td>ID WO</td>
                  <td><?php echo $isinya['id_wo']; ?></td>
                </tr>
                <tr>
                  <td>SO</td>
                  <td><?php echo $isinya['so']; ?></td>
                </tr>
                <tr>
                  <td>PIC INDOSAT</td>
                  <td><?php echo $isinya['pic_indosat']; ?></td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td><?php echo $isinya['location']; ?></td>
                </tr>
                <tr>
                  <td>Technician</td>
                  <td><?php echo $isinya['technician']; ?></td>
                </tr>
                <tr>
                  <td>Information</td>
                  <td><?php echo $isinya['information']; ?></td>
                </tr>
                <tr>
                  <td>Progress</td>
                  <td><?php echo $isinya['progress']; ?></td>
                </tr>
                <tr>
                  <td>CID</td>
                  <td><?php echo $isinya['cid']; ?></td>
                </tr>
                <tr>
                  <td>Date Submit Order</td>
                  <td><?php echo $isinya['created_at']; ?></td>
                </tr>
                <tr>
                  <td>Last Updated</td>
                  <td><?php echo $isinya['updated_at']; ?></td>
                </tr>
                <?php if ($isinya['finished_at'] == "0000-00-00"): ?>
                  <tr>
                    <td>Is Finished</td>
                    <td>No</td>
                  </tr>
                <?php else: ?>
                  <tr>
                    <td>Finished At</td>
                    <td><?php echo $isinya['finished_at']; ?></td>
                  </tr>
                <?php endif; ?>
                <tr>
                  <td>Report Teknis</td>
                  <td><?php echo $isinya['report_teknis']; ?></td>
                </tr>
                <tr>
                  <td>Price Codefication</td>
                  <td><?php echo $isinya['kodefikasi_harga']; ?></td>
                </tr>
                <tr>
                  <td>Prices</td>
                  <td><?php echo $isinya['harga']; ?></td>
                </tr>
                <tr>
                  <td>NO BOQ</td>
                  <td><?php echo $isinya['no_boq']; ?></td>
                </tr>
                <tr>
                  <td>NO BA</td>
                  <td><?php echo $isinya['no_ba']; ?></td>
                </tr>
                <tr>
                  <td>Work Name</td>
                  <td><?php echo $isinya['nama_pekerjaan']; ?></td>
                </tr>
                <tr>
                  <td>Price of Work</td>
                  <td><?php echo $isinya['nominal_pekerjaan']; ?></td>
                </tr>
                <tr>
                  <td>Submit Date BA</td>
                  <td><?php echo $isinya['tanggal_submit_ba']; ?></td>
                </tr>
                <tr>
                  <td>PIC Name</td>
                  <td><?php echo $isinya['nama_pic']; ?></td>
                </tr>
                <tr>
                  <td>Revision</td>
                  <td><?php echo $isinya['revisi']; ?></td>
                </tr>
                <tr>
                  <td>NO PB</td>
                  <td><?php echo $isinya['no_pb']; ?></td>
                </tr>
                <tr>
                  <td>NO SC</td>
                  <td><?php echo $isinya['no_sc']; ?></td>
                </tr>
                <tr>
                  <td>NO PO</td>
                  <td><?php echo $isinya['no_po']; ?></td>
                </tr>
                <tr>
                  <td>NO GR</td>
                  <td><?php echo $isinya['no_gr']; ?></td>
                </tr>
                <tr>
                  <td>Create Invoice</td>
                  <td><?php echo $isinya['create_invoice']; ?></td>
                </tr>
                <tr>
                  <td>Status Paid</td>
                  <td><?php echo $isinya['status_paid']; ?></td>
                </tr>
                <tr>
                  <td>Lending</td>
                  <td><?php echo $isinya['peminjaman']; ?></td>
                </tr>
                <tr>
                  <td>Detail Lending</td>
                  <td><?php echo $isinya['detail_peminjaman']; ?></td>
                </tr>
                <tr>
                  <td>Ops Fund</td>
                  <td><?php echo $isinya['dana_ops']; ?></td>
                </tr>
                <tr>
                  <td>Detail Ops Fund</td>
                  <td><?php echo $isinya['detail_dana_ops']; ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
        </div>
        <div class="row">
          <div class="container">
            <h5 class="center-align">Scheduling List</h5>
          </div>
        </div>
        <?php foreach ($isian as $jos): ?>
        <div class="row">
          <div class="container">
            <a href="#" class="waves-effect waves-light btn">Edit Scheduling</a>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <hr>
            <table class="bordered highlight responsive-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>ID Scheduling</td>
                  <td><?php echo $jos['id_scheduling']; ?></td>
                </tr>
                <tr>
                  <td>ID Project</td>
                  <td><?php echo $jos['id_project']; ?></td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td><?php echo $jos['tanggal']; ?>-<?php echo $jos['bulan']; ?>-<?php echo $jos['tahun']; ?></td>
                </tr>
                <tr>
                  <td>Project</td>
                  <td><?php echo $jos['project']; ?></td>
                </tr>
                <tr>
                  <td>Customer</td>
                  <td><?php echo $jos['customer']; ?></td>
                </tr>
                <tr>
                  <td>ID WO</td>
                  <td><?php echo $jos['id_wo']; ?></td>
                </tr>
                <tr>
                  <td>PIC INDOSAT</td>
                  <td><?php echo $jos['pic_indosat']; ?></td>
                </tr>
                <tr>
                  <td>Location</td>
                  <td><?php echo $jos['location']; ?></td>
                </tr>
                <tr>
                  <td>Technician</td>
                  <td><?php echo $jos['technician']; ?></td>
                </tr>
                <tr>
                  <td>Information</td>
                  <td><?php echo $jos['keterangan']; ?></td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td><?php echo $jos['status']; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</main>
