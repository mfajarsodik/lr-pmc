<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>super_admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/tech_log">Technician Log</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>super_admin/control_technician">Control Technician</a></li>
              <li><a class="active orange white-text" href="<?php echo base_url(); ?>super_admin/control_user">Control User</a></li>
              <li><a href="<?php echo base_url(); ?>super_admin/control_project">Control Project</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>super_admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <div class="row">
      <a class="waves-effect waves-light teal btn" href="<?php echo base_url(); ?>super_admin/add_user">Add User</a>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <table class="highlight responsive-table bordered">
          <thead>
            <tr>
              <th>ID User</th>
              <th>Username</th>
              <th>Name</th>
              <th>Position</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($isi as $isinya) : ?>
            <tr>
              <td><?php echo $isinya['id_user']; ?></td>
              <td><?php echo $isinya['username']; ?></td>
              <td><?php echo $isinya['name']; ?></td>
              <td><?php echo $isinya['position']; ?></td>
              <td>
                <a class="dropdown-button btn" data-activates="<?php echo $isinya['id_user']; ?>">Action</a>
                <ul id="<?php echo $isinya['id_user']; ?>" class="dropdown-content">
                  <li><a href="edit_user/<?php echo $isinya['id_user']; ?>">Edit</a></li>
                  <li><a href="delete_user/<?php echo $isinya['id_user']; ?>">Delete</a></li>
                </ul>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</main>
