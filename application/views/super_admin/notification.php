<?php if ($tanggal == 'NULL') : ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>super_admin">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <?php
            $query = $this->db->query("SELECT list_project.id_project, list_project.updated_at FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.kodefikasi_harga = ' '");
            $row = $query->num_rows();
            if ($row > 0) : ?>
              <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>super_admin/graphic">Graphic Project</a></li>
                    <li><a class="orange white-text" href="<?php echo base_url(); ?>super_admin/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                  </ul>
                </div>
              </li>
            <?php else: ?>
              <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>super_admin/graphic">Graphic Project</a></li>
                    <li><a class="orange white-text" href="<?php echo base_url(); ?>super_admin/notification">Notification</a></li>
                  </ul>
                </div>
              </li>
            <?php endif; ?>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>super_admin/book_of_life">Book Of Life</a></li>
                  <li><a href="<?php echo base_url(); ?>super_admin/tech_log">Technician Log</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/control_technician">Control Technician</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/control_user">Control User</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/control_project">Control Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>super_admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row white z-depth-2">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Notification</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>super_admin/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Finished All Report WO"></span>Hello Admin Was</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>super_admin/list_codefication" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Codefication"></span>Hello, Tell PM About</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
<?php else : ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>super_admin">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>super_admin/graphic">Graphic Project</a></li>
                  <li><a class="orange white-text" href="<?php echo base_url(); ?>super_admin/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>super_admin/book_of_life">Book Of Life</a></li>
                  <li><a href="<?php echo base_url(); ?>super_admin/tech_log">Technician Log</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/control_technician">Control Technician</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/control_user">Control User</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/control_project">Control Project</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>super_admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row white z-depth-2">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Notification</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>super_admin/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello, Tell Admin About</a>
              </div>
            </div>
          </div>
          <?php if (condition): ?>

          <?php endif; ?>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>super_admin/list_codefication" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Codefication">Hello, Tell PM About</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
<?php endif; ?>
<!-- <span class="new badge white black-text" data-badge-caption="New"></span> -->
