<header>
  <div class="container">
    <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
  </div>
  <ul id="nav-mobile" class="side-nav fixed">
    <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>super_admin">
      <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
        Your browser does not support SVG
      </object>
    </a></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion pad-20px">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/graphic">Graphic Project</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/notification">Notification</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Export Data</a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php echo base_url(); ?>super_admin/book_of_life">Book Of Life</a></li>
                <li><a href="<?php echo base_url(); ?>super_admin/tech_log">Technician Log</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li class="bold"><a class="collapsible-header waves-effect waves-orange">Control</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="<?php echo base_url(); ?>super_admin/control_technician">Control Technician</a></li>
              <li><a class="active orange white-text" href="<?php echo base_url(); ?>super_admin/control_user">Control User</a></li>
              <li><a href="<?php echo base_url(); ?>super_admin/control_project">Control Project</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li class="no-padding bottom"><a href="<?php echo base_url(); ?>super_admin/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
</header>
<main>
  <div class="container section">
    <form class="white z-depth-2" action="<?php echo base_url(); ?>super_admin/act_edit_user/<?php echo $id_user; ?>" method="post" accept-charset="utf-8">
      <div class="submit-project">
        <div class="row">
          <div class="container">
            <h5>Form Edit User</h5>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="id_user" value="<?php echo $id_user; ?>" disabled>
              <label for="id_user">ID User</label>
            </div>
          </div>
        </div>
        <?php foreach ($isi as $isinya) : ?>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="username" value="<?php echo $isinya['username'] ?>" class="validate">
              <label for="username">Username</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="password" name="password" class="validate">
              <label for="password">Password</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <input type="text" name="name" value="<?php echo $isinya['name']; ?>">
              <label for="name">Name</label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <div class="input-field">
              <select class="" name="position">
                <option value="<?php echo $isinya['position']; ?>" selected><?php echo $isinya['position']; ?></option>
                <?php foreach ($position as $position_name) : ?>
                  <option value="<?php echo $position_name['position_name']; ?>"><?php echo $position_name['position_name']; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container">
            <button class="btn waves-effect waves-light teal" type="submit" name="action">Submit</button>
          </div>
        </div>
      </div>
    </form>
      <?php endforeach; ?>
  </div>
</main>
