<?php if ($tanggal == "NULL"): ?>
  <?php
  $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.report_teknis = 'Done' AND confirmation_project.kodefikasi_harga = ' '");
  $row = $query->num_rows();
  if ($row > 0) :
  ?>
  <header>
    <div class="container">
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
    <ul id="nav-mobile" class="side-nav fixed">
      <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>pm">
        <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
          Your browser does not support SVG
        </object>
      </a></li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion pad-20px">
            <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>

              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>pm/graphic">Graphic Project</a></li>
                  <li class="active orange"><a href="<?php echo base_url(); ?>pm/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="<?php echo base_url(); ?>pm/manage_wo">Manage WO</a></li>
                </ul>
              </div>
            </li>
          </ul>
      </li>
      <li class="no-padding bottom"><a href="<?php echo base_url(); ?>pm/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
  </header>
  <main>
    <div class="container section">
      <div class="row white z-depth-2">
        <div class="submit-project">
          <div class="row">
            <div class="container">
              <h5>Notification</h5>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                  <a href="<?php echo base_url(); ?>pm/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello PM Admin Is Working On</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="container">
              <div class="collection">
                <a href="<?php echo base_url(); ?>pm/list_unfinished_codefication" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Price Codefication"><?php echo $row; ?></span>Hello PM You Have</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <?php else : ?>
    <header>
      <div class="container">
        <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
      </div>
      <ul id="nav-mobile" class="side-nav fixed">
        <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>pm">
          <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
            Your browser does not support SVG
          </object>
        </a></li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion pad-20px">
              <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>

                <div class="collapsible-body">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>pm/graphic">Graphic Project</a></li>
                    <li class="active orange"><a href="<?php echo base_url(); ?>pm/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                  </ul>
                </div>
              </li>
            </ul>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
              <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="<?php echo base_url(); ?>pm/manage_wo">Manage WO</a></li>
                  </ul>
                </div>
              </li>
            </ul>
        </li>
        <li class="no-padding bottom"><a href="<?php echo base_url(); ?>pm/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
    </header>
    <main>
      <div class="container section">
        <div class="row white z-depth-2">
          <div class="submit-project">
            <div class="row">
              <div class="container">
                <h5>Notification</h5>
              </div>
            </div>
            <div class="row">
              <div class="container">
                <div class="collection">
                    <a href="<?php echo base_url(); ?>pm/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello PM Admin Is Working On</a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="container">
                <div class="collection">
                  <a href="<?php echo base_url(); ?>pm/list_unfinished_codefication" class="collection-item"><span class="new badge" data-badge-caption="Finished All Price Codefication"><?php echo $row; ?></span>Hello PM You Were</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  <?php endif; ?>

<?php else: ?>

  <?php
  $tglProject = new DateTime($tanggal);
  $now = new DateTime("now");
  if ($tglProject->diff($now) >= "2 days") :
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project on list_project.id_project = confirmation_project.id_project WHERE confirmation_project.report_teknis = 'Done' AND confirmation_project.kodefikasi_harga = ' '");
    // cek notif buat si pm
    $row = $query->num_rows(); ?>
    <?php  if ($row > 0) : ?>
      <header>
        <div class="container">
          <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
        </div>
        <ul id="nav-mobile" class="side-nav fixed">
          <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>pm">
            <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
              Your browser does not support SVG
            </object>
          </a></li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion pad-20px">
                <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>

                  <div class="collapsible-body">
                    <ul>
                      <li><a href="<?php echo base_url(); ?>pm/graphic">Graphic Project</a></li>
                      <li class="active orange"><a href="<?php echo base_url(); ?>pm/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                    </ul>
                  </div>
                </li>
              </ul>
          </li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
                  <div class="collapsible-body">
                    <ul>
                      <li><a href="<?php echo base_url(); ?>pm/manage_wo">Manage WO</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
          </li>
          <li class="no-padding bottom"><a href="<?php echo base_url(); ?>pm/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
      </header>
      <main>
        <div class="container section">
          <div class="row white z-depth-2">
            <div class="submit-project">
              <div class="row">
                <div class="container">
                  <h5>Notification</h5>
                </div>
              </div>
              <div class="row">
                <div class="container">
                  <div class="collection">
                      <a href="<?php echo base_url(); ?>pm/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello PM, Tell Admin About</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="container">
                  <div class="collection">
                    <a href="<?php echo base_url(); ?>pm/list_unfinished_codefication" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Price Codefication"><?php echo $row; ?></span>Hello PM You Have</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    <?php else: ?>

      <header>
        <div class="container">
          <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only"><i class="material-icons">menu</i></a>
        </div>
        <ul id="nav-mobile" class="side-nav fixed">
          <li class="logo"><a id="logo-container" class="brand-logo" href="<?php echo base_url(); ?>pm">
            <object id="front-page-logo" type="image/svg+xml" data="<?php echo base_url(); ?>assets/image/lrlogo.svg">
              Your browser does not support SVG
            </object>
          </a></li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion pad-20px">
                <li class="bold"><a class="collapsible-header  waves-effect waves-orange">Dashboard<span class="new badge white black-text" data-badge-caption="New"></span></a>

                  <div class="collapsible-body">
                    <ul>
                      <li><a href="<?php echo base_url(); ?>pm/graphic">Graphic Project</a></li>
                      <li class="active orange"><a href="<?php echo base_url(); ?>pm/notification">Notification<span class="new badge white black-text" data-badge-caption="New"></span></a></li>
                    </ul>
                  </div>
                </li>
              </ul>
          </li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-orange">Task</a>
                  <div class="collapsible-body">
                    <ul>
                      <li><a href="<?php echo base_url(); ?>pm/manage_wo">Manage WO</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
          </li>
          <li class="no-padding bottom"><a href="<?php echo base_url(); ?>pm/logout" class="waves-effect waves-teal orange darken-2 white-text">Logout</a></li>
      </header>
      <main>
        <div class="container section">
          <div class="row white z-depth-2">
            <div class="submit-project">
              <div class="row">
                <div class="container">
                  <h5>Notification</h5>
                </div>
              </div>
              <div class="row">
                <div class="container">
                  <div class="collection">
                      <a href="<?php echo base_url(); ?>pm/list_unfinished_report" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Report WO"></span>Hello PM, Tell Admin About</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="container">
                  <div class="collection">
                    <a href="<?php echo base_url(); ?>pm/list_unfinished_codefication" class="collection-item"><span class="new badge" data-badge-caption="Unfinished Price Codefication"><?php echo $row; ?></span>Hello PM You Have</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

    <?php endif; ?>

  <?php endif; ?>

<?php endif; ?>
