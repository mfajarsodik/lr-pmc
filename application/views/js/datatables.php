<script type="text/javascript">
	$(document).ready(function(){
		var datatable = $('#list_wo').DataTable({
			"processing": true,
			"serverSide": true,
			"order": [],
			"ajax": {
				url: "<?php echo base_url() . 'leader/fetch_listwo'; ?>",
				type: "POST"
			},
			"columnDefs": [
				{
					"target": [0, 3, 4],
					"orderable": false
					
				}
			]
		});
	});
</script>