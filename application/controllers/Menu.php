<?php
/**
 *
 */
class Menu extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != 'lum#!' || $_SERVER['PHP_AUTH_PW'] != 'bun921') {
	      	header('WWW-Authenticate: Basic realm="MyProject"');
	      	header('HTTP/1.0 401 Unauthorized');
	      	die('Access Denied');    	}
  }

  public function index()
	{
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      redirect('admin');
    } else if ($status == "Finance") {
      redirect('finance');
    } else if ($status == "Leader") {
      redirect('leader');
    } else if ($status == "Super Admin") {
      redirect('super_admin');
    } else if ($status == "PM") {
      redirect('pm');
    } else if ($status == "Collection") {
      redirect('collection');
    } else {
      $this->login();
    }
	}

  public function login()
  {
    $this->load->view('templates/header');
		$this->load->view('lander/login');
		$this->load->view('templates/footer');
  }

  public function login_valid()
  {
    $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[18]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[18]');
    if ($this->form_validation->run() == FALSE) {
			$this->login();
		} else {
      $username = $this->input->post('username');
      $passwordnya = $this->input->post('password');
      $password_hash = password_hash($passwordnya, PASSWORD_BCRYPT);
      $user_id = $this->t_model->login($username);
      if ($user_id) {
        $query = $this->db->query("SELECT * FROM user WHERE id_user = '$user_id' ");
        $row = $query->row();
        if (isset($row)) {
          if ($password = password_verify($passwordnya, $row->password)) {
          //   $data = array(
          //     'id_user' => $row->id_user,
          //     'position' => $row->position
          // );
          // $this->session->set_userdata($data);
          if ($row->position == "Admin") {
            $data = array(
              'id_user' => $row->id_user,
              'position' => $row->position
            );
            $this->session->set_userdata($data);
            redirect('admin');
          } elseif ($row->position == "Leader") {
            $data = array(
              'id_user' => $row->id_user,
              'position' => $row->position
            );
            $this->session->set_userdata($data);
            redirect('leader');
          } elseif ($row->position == "Finance") {
            $data = array(
              'id_user' => $row->id_user,
              'position' => $row->position
            );
            $this->session->set_userdata($data);
            redirect('finance');
          } elseif ($row->position == "Super Admin") {
            $data = array(
              'id_user' => $row->id_user,
              'position' => $row->position
            );
            $this->session->set_userdata($data);
            redirect('super_admin');
          } elseif ($row->position == "PM") {
            $data = array(
              'id_user' => $row->id_user,
              'position' => $row->position
            );
            $this->session->set_userdata($data);
            redirect('pm');
          } elseif ($row->position == "Collection") {
            $data = array(
              'id_user' => $row->id_user,
              'position' => $row->position
            );
            $this->session->set_userdata($data);
            redirect('collection');
          }else {
            show_404();
          }
        } else {
          $this->session->set_flashdata('login_failed','Login failed, Username / password is wrong');
          redirect('menu');
        }
        } else {
          show_404();
        }
      } else {
        $this->session->set_flashdata('login_failed','Login failed, Username / password is wrong');
        redirect('menu');
      }
    }
  }

  public function signup()
  {
    $data['position_name'] = $this->t_model->getPosition();

    $this->load->view('templates/header');
		$this->load->view('lander/signup', $data);
		$this->load->view('templates/footer');
  }

  public function signup_valid()
  {
    $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[18]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[18]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[35]');
    if ($this->form_validation->run() === FALSE) {
			$this->signup();
		} else {
      $password = $this->input->post('password');
			$enc_password = password_hash($password, PASSWORD_BCRYPT);
			$this->t_model->register($enc_password);
			$username = $this->input->post('username');
			$name = $this->input->post('name');
			$position = $this->input->post('position');
			$this->session->set_flashdata('user_registered', 'You are now registered');
			$this->login();
    }
  }

}
 ?>
