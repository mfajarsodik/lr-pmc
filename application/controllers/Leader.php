<?php
/**
 *
 */
class Leader extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    header('Cache-Control: no-cache, must-revalidate');
  }
  // TODO: tambahin codingan construct ke semua controller buat auto resubmission

  public function index()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      $this->dashboard();
    } else {
      redirect('menu');
    }
  }

  public function list_wo()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      
      $data['isi'] = $this->t_model->getDoubleIdWoAndSo();

      $query = $this->db->query("SELECT id_wo, so, project, technician, tanggal, bulan, tahun ,COUNT(*) c FROM list_project WHERE id_wo OR SO IS NOT NULL GROUP BY id_wo, so HAVING c > 1");

      $config['base_url'] = base_url()."leader/list_wo";
      $config['total_rows'] = $query->num_rows();
      $config['per_page'] = 5;

      $config['full_tag_open'] = "<ul class=\"pagination\">";
      $config['full_tag_close'] = "</ul>";

      $config['next_tag_open'] = "<li class=\"waves-effect\">";
      $config['prev_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_open'] = "<li class=\"waves-effect\">";
      $config['last_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_close'] = "</li>";
      $config['last_tag_close'] = "</li>";

      $config['num_tag_open'] = "<li class=\"waves-effect\">";
      $config['num_tag_close'] = "</li>";

      $config['next_tag_close'] = "</li>";
      $config['prev_tag_close'] = "</li>";

      $config['cur_tag_open'] = "<li class=\"disabled\"><a class=\"red white-text\">";
      $config['cur_tag_close'] = "</a></li>";

      $this->pagination->initialize($config);
      $this->load->view('templates/header');
      $this->load->view('leader/list_wo',$data);
      $this->load->view('templates/footer');
      // yang dibawah plann pake datatable
      // $this->load->view('templates/header');
      // $this->load->view('leader/data_listwo');
      // $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function fetch_listwo()
  {
    
  }

  public function detail_with_id_wo($id_wo)
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      if (strpos($id_wo, '%C2%A') !== FALSE) {
        $first_filter = str_replace('%C2%A0', '_', $id_wo);
        $data['isi'] = $this->t_model->getDetailDoubleIdWo($first_filter);
        $this->load->view('templates/header');
        $this->load->view('leader/detail_double_id_wo', $data);
        $this->load->view('templates/footer');
      }
      $id_wo_filter = str_replace('%20', ' ', $id_wo);
      $data['isi'] = $this->t_model->getDetailDoubleIdWo($id_wo_filter);
      $this->load->view('templates/header');
      $this->load->view('leader/detail_double_id_wo', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_with_id_wo_from_null()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      $this->load->view('templates/header');
      $this->load->view('leader/detail_double_id_wo_null');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function double_id_wo()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      $this->load->view('templates/header');
      $this->load->view('leader/double_id_wo');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function double_so()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      $this->load->view('templates/header');
      $this->load->view('leader/double_so');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function dashboard()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      $this->load->view('templates/header');
      $this->load->view('leader/dashboard');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function notification()
  {
    $status = $this->session->userdata('position');
    if ($status == "Leader") {
      $query = $this->db->query(
        "SELECT list_project.id_project, list_project.updated_at FROM list_project
        LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project
        WHERE list_project.progress = 'Done'
        AND confirmation_project.report_teknis IS NULL
        OR confirmation_project.report_teknis = ' '
        ORDER BY list_project.updated_at ASC");
      $row = $query->row();
      if (isset($row)) {
        $data = array(
          'id_project' => $row->id_project,
          'tanggal' => $row->updated_at
        );
      } else {
        $data = array(
          'id_project' => 'NULL',
          'tanggal' => 'NULL'
        );
      }
      $this->load->view('templates/header');
      $this->load->view('leader/notification',$data);
      $this->load->view('templates/footer');
    }
  }

  public function list_unfinished_report()
  {
    $query = $this->db->query(
      "SELECT list_project.id_project, list_project.updated_at FROM list_project
      LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project
      WHERE list_project.progress = 'Done'
      AND confirmation_project.report_teknis IS NULL ORDER BY list_project.updated_at ASC");
    $data = array('isian' => $query->num_rows());
    $data['isi'] = $this->t_model->getListForNotif();
    $this->load->view('templates/header');
    $this->load->view('leader/list_unfinished_report',$data);
    $this->load->view('templates/footer');
  }

  public function detail_order($id_project)
  {
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $this->load->view('templates/header');
    $this->load->view('leader/detail_order',$data);
    $this->load->view('templates/footer');
  }

  public function detail_progress($id_project)
  {
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $this->load->view('templates/header');
    $this->load->view('leader/detail_progress',$data);
    $this->load->view('templates/footer');
  }

  public function continue_wo()
  {
    $data['isi'] = $this->t_model->getListWoNullTechnician();

    $this->db->where('technician', ' ');
    $query = $this->db->get('list_project');

    $config['base_url'] = base_url()."leader/continue_wo/";
    $config['total_rows'] = $query->num_rows();
    $config['per_page'] = 5;

    $config['full_tag_open'] = "<ul class=\"pagination\">";
    $config['full_tag_close'] = "</ul>";

    $config['next_tag_open'] = "<li class=\"waves-effect\">";
    $config['prev_tag_open'] = "<li class=\"waves-effect\">";

    $config['first_tag_open'] = "<li class=\"waves-effect\">";
    $config['last_tag_open'] = "<li class=\"waves-effect\">";

    $config['first_tag_close'] = "</li>";
    $config['last_tag_close'] = "</li>";

    $config['num_tag_open'] = "<li class=\"waves-effect\">";
    $config['num_tag_close'] = "</li>";

    $config['next_tag_close'] = "</li>";
    $config['prev_tag_close'] = "</li>";

    $config['cur_tag_open'] = "<li class=\"disabled\"><a class=\"red white-text\">";
    $config['cur_tag_close'] = "</a></li>";

    $this->pagination->initialize($config);

    $this->load->view('templates/header');
    $this->load->view('leader/continue_wo',$data);
    $this->load->view('templates/footer');
  }

  public function search_progress()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_wo', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('leader/progress_wo');
    } else {
      if ($this->input->post('filter') == "Date") {
        $str = $this->input->post('search_wo');
        $arr = explode(' ', $str);
        for ($i = 0; isset($arr[$i]); $i++) ${'isi'.($i + 1)} = $arr[$i];
        if ($isi2 == NULL) {
          $this->session->set_flashdata('input_bulan', 'Please input month');
          redirect('leader/progress_wo');
        } else if ($isi3 == NULL) {
          $this->session->set_flashdata('input_tahun', 'Please input year');
          redirect('leader/progress_wo');
        } else {
          $this->search_date_progress($isi1,$isi2,$isi3);
        }
      } else if ($this->input->post('filter') == "ID WO") {
        $str = $this->input->post('search_wo');
        $this->search_by_idwo_progress($str);
      } else if ($this->input->post('filter') == "Project") {
        $str = $this->input->post('search_wo');
        $this->search_by_project_progress($str);
      } else if ($this->input->post('filter') == "Customer") {
        $str = $this->input->post('search_wo');
        $this->search_by_customer_progress($str);
      }
    }
  }

  public function search_date_progress($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3' AND technician != ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterDateTechnicianNotNull($isi1,$isi2,$isi3);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search_progress', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_idwo_progress($str)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE id_wo LIKE '$str' AND technician != ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterWoLeader($str);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search', $data);
    $this->load->view('templates/footer');
  }
  // TODO: search di leader cek lagi yang di continue wo sama progress wo
  // TODO: buat pagination di tiap fungsi
  // TODO: buat search di scheduling wo
  // TODO: fungsi di scheduling wo form sama fungsinya blm dibikin
  // TODO: notification tiap muncul order baru

  public function search_by_project_progress($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos' AND technician != ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByProjectTechnicianNotNull($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search_progress', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_customer_progress($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE customer RLIKE '$jos' AND technician !=' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByCustomerTechnicianNotNull($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search_progress', $data);
    $this->load->view('templates/footer');
  }

  public function search_continue()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_wo', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('leader/continue_wo');
    } else {
      if ($this->input->post('filter') == "Date") {
        $str = $this->input->post('search_wo');
        $arr = explode(' ', $str);
        for ($i = 0; isset($arr[$i]); $i++) ${'isi'.($i + 1)} = $arr[$i];
        if ($isi2 == NULL) {
          $this->session->set_flashdata('input_bulan', 'Please input month');
          redirect('leader/continue_wo');
        } else if ($isi3 == NULL) {
          $this->session->set_flashdata('input_tahun', 'Please input year');
          redirect('leader/continue_wo');
        } else {
          $this->search_date($isi1,$isi2,$isi3);
        }
      } else if ($this->input->post('filter') == "ID WO") {
        $str = $this->input->post('search_wo');
        $this->search_by_idwo($str);
      } else if ($this->input->post('filter') == "Project") {
        $str = $this->input->post('search_wo');
        $this->search_by_project($str);
      } else if ($this->input->post('filter') == "Customer") {
        $str = $this->input->post('search_wo');
        $this->search_by_customer($str);
      }
    }
  }

  public function search_by_customer($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE customer RLIKE '$jos' AND technician = ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByCustomerLeader($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_project($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos' AND technician = ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByProjectLeader($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_idwo($str)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE id_wo LIKE '$str' AND technician = ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterWoLeader($str);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_date($isi1, $isi2, $isi3)
  {
    $query = $this->db->query(
      "SELECT * FROM list_project
      WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3' AND technician = ' '");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterDateLeader($isi1,$isi2,$isi3);
    $this->load->view('templates/header');
    $this->load->view('leader/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function logout()
  {
    $this->session->unset_userdata('position');
    $this->session->unset_userdata('id_user');
    $this->index();
  }

  public function reschedule_order_wo($id_project)
  {
    $id_project = $this->uri->segment(3);
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $data['teknisi'] = $this->t_model->getTechincianName();
    $this->load->view('templates/header');
    // print_r($data);
    $this->load->view('leader/form_reschedule_wo',$data);
    $this->load->view('templates/footer');
  }

  public function act_reschedule_order_wo($id_project)
  {
    $teknisinya = $this->input->post('technician');
    $jos = implode(", ", $teknisinya);

    $this->t_model->scheduleOrderWo($id_project, $jos);
    redirect('leader');
  }

  public function add_technician_for_order_wo($id_project)
  {
    $id_project = $this->uri->segment(3);
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $data['teknisi'] = $this->t_model->getTechincianName();
    $this->load->view('templates/header');
    $this->load->view('leader/form_add_technician_for_wo',$data);
    $this->load->view('templates/footer');
  }

  public function act_add_technician_for_order_wo($id_project)
  {
    $teknisinya = $this->input->post('technician');
    $jos = implode(", ", $teknisinya);

    $this->t_model->addTechnicianForOrderWo($id_project, $jos);
    $this->index();
  }

  public function scheduling_wo()
  {
    $data['isinya'] = $this->t_model->getListUnscheduledWo();
    $data['isi'] = $this->t_model->cekSchedulingWo();
    $this->load->view('templates/header');
    $this->load->view('leader/scheduling_wo',$data);
    $this->load->view('templates/footer');
  }

  public function schedule_order_wo($id_project)
  {
    $data['isi'] = $id_project;
    $this->load->view('templates/header');
    $this->load->view('leader/schedule_order_wo',$data);
    $this->load->view('templates/footer');
  }

  public function search_unscheduled()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_wo', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('leader/scheduling_wo/#test-swipe-2');
    } else {
      if ($this->input->post('filter') == "Date") {
        $str = $this->input->post('search_wo');
        $arr = explode(' ', $str);
        for ($i = 0; isset($arr[$i]); $i++) ${'isi'.($i + 1)} = $arr[$i];
        if ($isi2 == NULL) {
          # code...
        }
      } else if ($this->input->post('filter') == "Project") {
        # code...
      } else if ($this->input->post('filter') == "ID WO") {
        # code...
      } else if ($this->input->post('filter') == "Customer") {
        # code...
      } else if ($this->input->post('filter') == "PIC Indosat") {
        # code...
      }
    }
  }

  public function search_scheduled()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_wo', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('leader/scheduling_wo/');
    } else {
      if ($this->input->post('filter') == "Date") {
        $str = $this->input->post('search_wo');
        $arr = explode(' ', $str);
        for ($i = 0; isset($arr[$i]); $i++) ${'isi'.($i + 1)} = $arr[$i];
        if ($isi2 == NULL) {
          $this->session->set_flashdata('input_bulan', 'Please input month');
          redirect('leader/scheduling_wo');
        } else if ($isi3 == NULL) {
          $this->session->set_flashdata('input_tahun', 'Please input year');
          redirect('leader/scheduling_wo');
        } else {
          $this->search_scheduled_date($isi1,$isi2,$isi3);
        }
      } else if ($this->input->post('filter') == "Project") {
        $str = $this->input->post('search_wo');
        $this->search_scheduled_project($str);
      } else if ($this->input->post('filter') == "ID WO") {
        $str = $this->input->post('search_wo');
        $this->search_scheduled_idwo($str);
      } else if ($this->input->post('filter') == "Customer") {
        $str = $this->input->post('search_wo');
        $this->search_scheduled_customer($str);
      } else if ($this->input->post('filter') == "PIC Indosat") {
        $str = $this->input->post('search_wo');
        $this->search_scheduled_pic($str);
      } else if ($this->input->post('filter') == "Technician") {
        $str = $this->input->post('search_wo');
        $this->search_scheduled_technician($str);
      }
    }
  }

  public function search_scheduled_technician($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.technician RLIKE '$jos'
    ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterTechnicianScheduled($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/result_search_scheduled',$data);
    $this->load->view('templates/footer');
  }

  public function search_scheduled_pic($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.pic_indosat RLIKE '$jos'
      ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterPicScheduled($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/result_search_scheduled',$data);
    $this->load->view('templates/footer');
  }

  public function search_scheduled_customer($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.customer RLIKE '$jos'
    ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterCustomerScheduled($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/result_search_scheduled',$data);
    $this->load->view('templates/footer');
  }

  public function search_scheduled_idwo($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.id_wo RLIKE '$jos'
      ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterIdWoScheduled($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/result_search_scheduled',$data);
    $this->load->view('templates/footer');
  }

  public function search_scheduled_project($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.project RLIKE '$jos'
    ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getListFilterProjectScheduled($jos);
    $this->load->view('templates/header');
    $this->load->view('leader/result_search_scheduled',$data);
    $this->load->view('templates/footer');
  }

  public function search_scheduled_date($isi1, $isi2, $isi3)
  {
    $query = $this->db->query(
    "SELECT * FROM list_project JOIN scheduling_project
    ON list_project.id_project = scheduling_project.id_project
    WHERE list_project.technician != ' '
    AND scheduling_project.tanggal RLIKE '$isi1'
    AND scheduling_project.bulan RLIKE '$isi2'
    AND scheduling_project.tahun RLIKE '$isi3'
    ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterDateScheduled($isi1,$isi2,$isi3);
    $this->load->view('templates/header');
    $this->load->view('leader/result_search_scheduled',$data);
    $this->load->view('templates/footer');
  }

  public function detail_schedule($id_scheduling)
  {
    $data['isi'] = $this->t_model->getListDetailSChedule($id_scheduling);
    $this->load->view('templates/header');
    $this->load->view('leader/detail_schedule',$data);
    $this->load->view('templates/footer');
  }

  public function view_schedule_order_wo($id_project)
  {
    $data = array('id_project' => $id_project);
    $data['isi'] = $this->t_model->getListScheduleWithDetailProject($id_project);
    $this->load->view('templates/header');
    $this->load->view('leader/view_schedule',$data);
    $this->load->view('templates/footer');
  }

  public function act_schedule_order($id_project)
  {
    $teknisinya = $this->input->post('technician');
    $jos = implode(", ", $teknisinya);
    $this->t_model->addScheduleProject($id_project, $jos);
    redirect('leader/scheduling_wo');
  }

  public function progress_wo()
  {
    $data['isi'] = $this->t_model->getListProject();

    $query = $this->db->query("SELECT * FROM list_project WHERE technician != ' '");

    $config['base_url'] = base_url()."leader/progress_wo/";
    $config['total_rows'] = $query->num_rows();
    $config['per_page'] = 5;

    $config['full_tag_open'] = "<ul class=\"pagination\">";
    $config['full_tag_close'] = "</ul>";

    $config['next_tag_open'] = "<li class=\"waves-effect\">";
    $config['prev_tag_open'] = "<li class=\"waves-effect\">";

    $config['first_tag_open'] = "<li class=\"waves-effect\">";
    $config['last_tag_open'] = "<li class=\"waves-effect\">";

    $config['first_tag_close'] = "</li>";
    $config['last_tag_close'] = "</li>";

    $config['num_tag_open'] = "<li class=\"waves-effect\">";
    $config['num_tag_close'] = "</li>";

    $config['next_tag_close'] = "</li>";
    $config['prev_tag_close'] = "</li>";

    $config['cur_tag_open'] = "<li class=\"disabled\"><a class=\"red white-text\">";
    $config['cur_tag_close'] = "</a></li>";

    $this->pagination->initialize($config);
    $this->load->view('templates/header');
    $this->load->view('leader/progress_wo',$data);
    $this->load->view('templates/footer');
  }

  public function set_progress_wo($id_project)
  {
    $id_project = $this->uri->segment(3);
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $this->load->view('templates/header');
    $this->load->view('leader/set_progress_wo',$data);
    $this->load->view('templates/footer');
  }

  public function act_set_progress_wo($id_project)
  {
    $this->t_model->setProgressWo($id_project);
    redirect('leader');
  }
  // TODO: leader continue wo data dari order yang paling baru
  // TODO: leader input teknisi baru di reschedule
  // TODO: reschedulenya bisa banyak ngga pake ckeditor
  // TODO: report leader sampai cid
  // TODO: leader muncul notif kalau ada order baru
  // TODO: leader bisa sort data kaya yang ada di excel

}

 ?>
