<?php
/**
 *
 */
class Admin extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    header('Cache-Control: no-cache, must-revalidate'); //biar ngga minta resend form submission
  }

  public function index()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $this->dashboard();
    } else {
      redirect('menu');
    }
  }

  public function dashboard()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
      $data['josy'] = $this->t_model->checkProjectIdForRandomWo();
      $data = array('isi' => $query->num_rows());
      $this->load->view('templates/header');
      $this->load->view('admin/dashboard',$data);
      print_r($data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function control_technician()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getTechnician();

      $this->db->order_by('name_technician');
      $query = $this->db->get('technician_lr');

      $config['base_url'] = base_url()."admin/control_technician";
      $config['total_rows'] = $query->num_rows();
      $config['per_page'] = 5;

      $config['full_tag_open'] = "<ul class=\"pagination\">";
      $config['full_tag_close'] = "</ul>";

      $config['next_tag_open'] = "<li class=\"waves-effect\">";
      $config['prev_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_open'] = "<li class=\"waves-effect\">";
      $config['last_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_close'] = "</li>";
      $config['last_tag_close'] = "</li>";

      $config['num_tag_open'] = "<li class=\"waves-effect\">";
      $config['num_tag_close'] = "</li>";

      $config['next_tag_close'] = "</li>";
      $config['prev_tag_close'] = "</li>";

      $config['cur_tag_open'] = "<li class=\"disabled\"><a class=\"red white-text\">";
      $config['cur_tag_close'] = "</a></li>";

      $this->pagination->initialize($config);

      $this->load->view('templates/header');
      $this->load->view('admin/control_technician', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function search_technician()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_technician', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('admin/control_technician');
    } else {
      if ($this->input->post('filter') == "NIK") {
        $str = $this->input->post('search_technician');
        $this->search_nik($str);
      } elseif ($this->input->post('filter') == "Name") {
        $str = $this->input->post('search_technician');
        $this->search_name($str);
      } elseif ($this->input->post('filter') == "Status") {
        $str = $this->input->post('search_technician');
        $this->search_status_technician($str);
      }
    }
  }

  public function search_nik($str)
  {
    $query = $this->db->query("SELECT * FROM technician_lr WHERE nik = '$str'");
    $data = array('isian' => $query->num_rows());
    $data['josy'] = $this->t_model->getFilterNIK($str);
    $this->load->view('templates/header');
    $this->load->view('admin/search_technician', $data);
    $this->load->view('templates/footer');
  }

  public function search_name($str)
  {
    $query = $this->db->query("SELECT * FROM technician_lr WHERE name_technician RLIKE '$str'");
    $data = array(
                  'isian' => $query->num_rows(),
                  'josy' => $this->t_model->checkProjectIdForRandomWo()
                  );
    $this->load->view('templates/header');
    $this->load->view('admin/search_technician', $data);
    $this->load->view('templates/footer');
  }

  public function search_status_technician($str)
  {
    $query = $this->db->query("SELECT * FROM technician_lr WHERE status RLIKE '$str'");
    $data = array('isian' => $query->num_rows());
    $data['josy'] = $this->t_model->getFilterStatusTechnician($str);
    $this->load->view('templates/header');
    $this->load->view('admin/search_technician', $data);
    $this->load->view('templates/footer');
  }

  public function edit_tech($id_technician)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data = array('id_technician' => $id_technician );
      $data['isi'] = $this->t_model->getDetailTechnician($id_technician);
      $this->load->view('templates/header');
      $this->load->view('admin/edit_tech', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_tech($id_technician)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $this->t_model->setEditTech($id_technician);
      redirect('admin/control_technician');
    } else {
      redirect('menu');
    }
  }

  public function add_tech()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $this->load->view('templates/header');
      $this->load->view('admin/add_tech');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_tech()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $this->t_model->setAddTech();
      redirect('admin/control_technician');
    } else {
      redirect('menu');
    }
  }

  public function delete_tech($id_technician)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $this->t_model->setDeleteTech($id_technician);
      redirect('admin/control_technician');
    } else {
      redirect('menu');
    }
  }

  public function logout()
  {
    $this->session->unset_userdata('position');
    $this->session->unset_userdata('id_user');
    $this->index();
  }

  public function search_project_in_order()
  {
    if (isset($_GET['term'])) {
      $result = $this->t_model->getProjectForSearch($_GET['term']);
      if (count($result) > 0) {
        foreach ($result as $pr) {
          $data[] = $pr->project;
          echo json_encode($data);
        }
      }
    }
  }

  public function search_customer_in_order()
  {
    if (isset($_GET['term'])) {
      $result = $this->t_model->getCustomerForSearch($_GET['term']);
      if (count($result) > 0) {
        foreach ($result as $pr) {
           $data[] = $pr->customer;
           echo json_encode($data);
        }
      }
    }
  }

  public function order()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
      $data = array('isi' => $query->num_rows());
      $this->load->view('templates/header');
      $this->load->view('admin/order_wo',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_order()
  {
    $id_wo = $this->input->post('id_wo');
    $so = $this->input->post('so');
    if ($id_wo == '') {
      if ($so == '') {
        $randIdWo = $this->t_model->checkProjectIdForRandomWo();
        if ($randIdWo) {
          $number = $randIdWo + 1;
          $finalRandId = "LR_RAND_ID_$number";
          $this->t_model->addOrderWithRandIdWo($finalRandId);
          redirect('admin');
        }
      } else {
        $cekExistSo = $this->t_model->cekExistSo($so);
        if ($cekExistSo === "DOUBLE") {
          echo "
            <script>
              alert('Data SO Telah ada')
              window.history.back()
            </script>
          ";
        } else {
          $this->t_model->addOrder();
          redirect('admin');
        }
      }
    } else {
      if ($so == '') {
        $cekExistIdWo = $this->t_model->checkIdWo($id_wo);
        if ($cekExistIdWo === "DOUBLE") {
          echo "
            <script>
              alert('Id Wo Telah ada')
              window.history.back()
            </script>
          ";
        } else {
          $this->t_model->addOrder();
          redirect('admin');
        }
      } else {
        $cekExistIdWoAndSo = $this->t_model->cekExistIdWoAndSo($id_wo, $so);
        if ($cekExistIdWoAndSo === "DOUBLE") {
          echo "
            <script>
              alert('Data Id Wo dan SO tsb telah ada')
              window.history.back()
            </script>
          ";
        } else {
          $this->t_model->addOrder();
          redirect('admin');
        }
      }
    }
  }

  public function graphic()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
      $data = array('isi' => $query->num_rows());
      $this->load->view('templates/header');
      $this->load->view('admin/graphic',$data);
      $this->load->view('templates/footer_chart');
    } else {
      redirect('menu');
    }
    // TODO: buat graphic dengan data yang real dan sesuai input pencarian
  }

  public function datagraphic()
  {
    // header('Content-Type: application/json');
    // $data[] = $this->t_model->getFirstGraph();
    // $json_awal = json_encode($data);
    // echo $json_awal;
    // $json_akhir = json_decode($json_awal);
    // print $json_akhir->{'COUNT(id_project)'};
    $json = file_get_contents('https://beta2.sbuxcard.com/exchange/znwfapi.php?r=login');
    $obj = json_encode($json, true);
    print_r($obj);
  }

  public function notification()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
      $data = array('isi' => $query->num_rows());
      $this->load->view('templates/header');
      $this->load->view('admin/notification',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_unfinished_report()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
      $data = array('isi' => $query->num_rows());
      $data['isian'] = $this->t_model->getListForNotif();
      $this->load->view('templates/header');
      $this->load->view('admin/list_unfinished',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }

  }
  public function detail_wo_done($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('admin/detail_wo_done',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function done_wo($id_project)
  {
    $this->t_model->setReportTeknisDone($id_project);
    redirect('admin');
  }

  public function manage_wo()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project WHERE technician != ' '");
      $data = array('isi' => $query->num_rows());
      $data['isian'] = $this->t_model->getListProject();
      $data['isian_acc'] = $this->t_model->getListWoNullTechnician();
      $this->db->where('technician !=', ' ');
      $query = $this->db->get('list_project');

      $config['base_url'] = base_url()."admin/manage_wo/";
      $config['total_rows'] = $query->num_rows();
      $config['per_page'] = 5;

      $config['full_tag_open'] = "<ul class=\"pagination\">";
      $config['full_tag_close'] = "</ul>";

      $config['next_tag_open'] = "<li class=\"waves-effect\">";
      $config['prev_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_open'] = "<li class=\"waves-effect\">";
      $config['last_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_close'] = "</li>";
      $config['last_tag_close'] = "</li>";

      $config['num_tag_open'] = "<li class=\"waves-effect\">";
      $config['num_tag_close'] = "</li>";

      $config['next_tag_close'] = "</li>";
      $config['prev_tag_close'] = "</li>";

      $config['cur_tag_open'] = "<li class=\"disabled\"><a class=\"red white-text\">";
      $config['cur_tag_close'] = "</a></li>";

      $this->pagination->initialize($config);

      $this->load->view('templates/header');
      $this->load->view('admin/manage_wo',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function search_filter()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_wo', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('admin/manage_wo');
    } else {
      if ($this->input->post('filter') == "Date") {
        $str = $this->input->post('search_wo');
        // $str = $this->input->post('filter');
        $arr = explode(' ', $str);
        for ($i = 0; isset($arr[$i]); $i++) ${'isi'.($i + 1)} = $arr[$i];
        $this->search_date($isi1,$isi2,$isi3);
      } else if ($this->input->post('filter') == "ID WO") {
        $str = $this->input->post('search_wo');
        $this->search_by_idwo($str);
      } else if ($this->input->post('filter') == "Project") {
        $str = $this->input->post('search_wo');
        $this->search_by_project($str);
      } else if ($this->input->post('filter') == "Technician") {
        $str = $this->input->post('search_wo');
        $this->search_by_technician($str);
      } else if ($this->input->post('filter') == "Customer") {
        $str = $this->input->post('search_wo');
        $this->search_by_customer($str);
      } else if ($this->input->post('filter') == "SO") {
        $str = $this->input->post('search_wo');
        $this->search_by_so($str);
      } else if ($this->input->post('filter') == "PIC INDOSAT") {
        $str = $this->input->post('search_wo');
        $this->search_by_pic_indosat($str);
      } else if ($this->input->post('filter') == "Location") {
        $str = $this->input->post('search_wo');
        $this->search_by_location($str);
      } else if ($this->input->post('filter') == "CID") {
        $str = $this->input->post('search_wo');
        $this->search_by_cid($str);
      }
    }
  }

  public function search_by_cid($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE cid RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByCid($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_location($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE location RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByLocation($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_pic_indosat($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE pic_indosat RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByPicIndosat($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_so($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE so RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterBySo($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_customer($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE customer RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByCustomer($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_date($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterDate($isi1,$isi2,$isi3);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_idwo($str)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE id_wo LIKE '$str' ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterWo($str);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_project($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByProject($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_technician($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE technician RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByTechnician($jos);
    $this->load->view('templates/header');
    $this->load->view('admin/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function detail_manage($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('admin/detail_manage_wo', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function edit_wo($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('admin/edit_wo', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_wo($id_project)
  {
    if ($this->input->post('progress') === "Pending") {
      $this->t_model->editOrder($id_project);
      $query = $this->db->query("SELECT * FROM reason_project WHERE id_project = $id_project");
      if ($query->num_rows() > 0) {
        redirect('admin');
      } else {
        $this->add_reason_information($id_project);
      }
    } else if($this->input->post('progress') === "Cancel") {
      $this->t_model->editOrder($id_project);
      $query = $this->db->query("SELECT * FROM reason_project WHERE id_project = $id_project");
      if ($query->num_rows() > 0) {
        redirect('admin');
      } else {
        $this->add_reason_information($id_project);
      }
    } else {
      $this->t_model->editOrder($id_project);
      redirect('admin/manage_wo');
    }
  }

  public function add_reason_information($id_project)
  {
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $this->load->view('templates/header');
    $this->load->view('admin/reason_information',$data);
    $this->load->view('templates/footer');
  }

  public function add_reason($id_project)
  {
    $this->t_model->addReason($id_project);
    redirect('admin/manage_wo');
  }

  public function manage_reason()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/footer');
    // TODO: buat tampilan untuk manage alasan dari project yang dicancel atau dipending
    // NOTE: jika status pending atau cancel diubah maka alasannya akan ikut terhapus
  }

  public function list_onprogress()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListOnProgress();
      $this->load->view('templates/header');
      $this->load->view('admin/list_onprogress',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_cancel()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListCancel();
      $this->load->view('templates/header');
      $this->load->view('admin/list_cancel',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_pending()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListPending();
      $this->load->view('templates/header');
      $this->load->view('admin/list_pending',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
  }
}

  public function list_done()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getListDone();
      $this->load->view('templates/header');
      $this->load->view('admin/list_done',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_reason()
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getReasonProject();
      $this->load->view('templates/header');
      $this->load->view('admin/list_reason',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_reason($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getReasonProjectWhere($id_project);
      // print_r($data);
      $this->load->view('templates/header');
      $this->load->view('admin/detail_reason', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function edit_reason($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $data['isi'] = $this->t_model->getReasonProjectWhere($id_project);
      // print_r($data);
      $this->load->view('templates/header');
      $this->load->view('admin/edit_reason', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_reason($id_project)
  {
    $this->t_model->editOrder($id_project);
    $this->t_model->setReason($id_project);
    redirect('admin/list_reason');
  }

  public function detail_pc_wo($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Admin") {
      $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
      $data = array('isi' => $query->num_rows());
      $data['isian'] = $this->t_model->getListReason($id_project);
      $this->load->view('templates/header');
      $this->load->view('admin/detail_pc_wo', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

}
// TODO: cek search di admin bener apa kaga
 ?>
