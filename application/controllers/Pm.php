<?php
/**
 *
 */
class Pm extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    header('Cache-Control: no-cache, must-revalidate');
  }

  public function index()
  {
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $this->dashboard();
    } else {
      redirect('menu');
    }
  }

  public function dashboard()
  {
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $query = $this->db->query("SELECT list_project.id_project, list_project.updated_at FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL ORDER BY list_project.updated_at ASC");
      $row = $query->row();
      if (isset($row)) {
        $data = array(
          'id_project' => $row->id_project,
          'tanggal' => $row->updated_at
        );
      } else {
        $data = array(
          'id_project' => "NULL",
          'tanggal' => "NULL"
        );
      }
      $this->load->view('templates/header');
      $this->load->view('pm/dashboard',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function notification()
  {
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $query = $this->db->query("SELECT list_project.id_project, list_project.updated_at FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL ORDER BY list_project.updated_at ASC");
      // NOTE: progress project done tapi blm buat report teknis
      $row = $query->row();
      if (isset($row)) {
        $data = array(
          'id_project' => $row->id_project,
          'tanggal' => $row->updated_at
        );
      }else {
        $data = array(
          'id_project' => 'NULL',
          'tanggal' => 'NULL'
        );
      }
      $this->load->view('templates/header');
      $this->load->view('pm/notification', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
    // TODO: notif tidak muncul,buat muncul
    // NOTE: notifnya muncul kalo H+2 dari done
    // NOTE: bedanya pm sama leader pm itu teknis, kalo leader bukan teknis
  }

  public function logout()
  {
    $this->session->unset_userdata('position');
    $this->session->unset_userdata('id_user');
    $this->index();
  }

  public function manage_wo()
  {
    $data['isi'] = $this->t_model->getListProject();
    $this->load->view('templates/header');
    $this->load->view('pm/manage_wo',$data);
    $this->load->view('templates/footer');
  }

  public function search_filter()
  {
    $this->form_validation->set_rules('filter', 'Filter', 'trim|required');
    $this->form_validation->set_rules('search_wo', 'Search WO', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('search_wrong', 'Please input what you want to search');
      redirect('pm/manage_wo');
    } else {
      if ($this->input->post('filter') == "Date") {
        $str = $this->input->post('search_wo');
        $arr = explode(' ', $str);
        for ($i = 0; isset($arr[$i]); $i++) ${'isi'.($i + 1)} = $arr[$i];
        $this->search_date($isi1, $isi2, $isi3);
      } else if ($this->input->post('filter') == "ID WO") {
        $str = $this->input->post('search_wo');
        $this->search_by_idwo($str);
      } else if ($this->input->post('filter') == "Project") {
        $str = $this->input->post('search_wo');
        $this->search_by_project($str);
      } else if ($this->input->post('filter') == "Technician") {
        $str = $this->input->post('search_wo');
        $this->search_by_technician($str);
      }
    }
  }

  public function search_date($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterDate($isi1,$isi2,$isi3);
    $this->load->view('templates/header');
    $this->load->view('pm/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_idwo($str)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE id_wo LIKE '$str' ");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterWo($str);
    $this->load->view('templates/header');
    $this->load->view('pm/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_project($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByProject($jos);
    $this->load->view('templates/header');
    $this->load->view('pm/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function search_by_technician($str)
  {
    $awal = '[[:<:]]';
    $akhir = '[[:>:]]';
    $jos = $awal.$str.$akhir;
    $query = $this->db->query("SELECT * FROM list_project WHERE technician RLIKE '$jos'");
    $data = array('isian' => $query->num_rows() );
    $data['josy'] = $this->t_model->getFilterByTechnician($jos);
    $this->load->view('templates/header');
    $this->load->view('pm/list_search', $data);
    $this->load->view('templates/footer');
  }

  public function list_onprogress()
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE progress = 'On Progress' AND technician != ' ' ");
    $data = array('mantap' => $query->num_rows() );
    $data['isi'] = $this->t_model->getListOnProgress();
    $this->load->view('templates/header');
    $this->load->view('pm/list_onprogress',$data);
    $this->load->view('templates/footer');
  }

  public function list_cancel()
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE progress = 'Cancel' AND technician != ' ' ");
    $data = array('mantap' => $query->num_rows() );
    $data['isi'] = $this->t_model->getListCancel();
    $this->load->view('templates/header');
    $this->load->view('pm/list_cancel',$data);
    $this->load->view('templates/footer');
  }

  public function list_done()
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE progress = 'Done' AND technician != ' ' ");
    $data = array('mantap' => $query->num_rows() );
    $data['isi'] = $this->t_model->getListDone();
    $this->load->view('templates/header');
    $this->load->view('pm/list_done',$data);
    $this->load->view('templates/footer');
  }

  public function list_pending()
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE progress = 'Pending' AND technician != ' ' ");
    $data = array('mantap' => $query->num_rows() );
    $data['isi'] = $this->t_model->getListPending();
    $this->load->view('templates/header');
    $this->load->view('pm/list_pending',$data);
    $this->load->view('templates/footer');
  }

  public function list_unfinished_report()
  {
    $data['isi'] = $this->t_model->getListForNotif();
    $this->load->view('templates/header');
    $this->load->view('pm/list_unfinished_report',$data);
    $this->load->view('templates/footer');
  }

  public function list_unfinished_codefication()
  {
    $data['isi'] = $this->t_model->getListUnfinishedCodefication();
    $this->load->view('templates/header');
    $this->load->view('pm/list_unfinished_codefication',$data);
    $this->load->view('templates/footer');
  }

  public function detail_unfinished($id_project)
  {
    $data['isi'] = $this->t_model->getListWoDetail($id_project);
    $this->load->view('templates/header');
    $this->load->view('pm/detail_unfinished_codefication',$data);
    $this->load->view('templates/footer');
  }

  public function done_codefication($id_project)
  {
    // TODO: tambahin input data harga project
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('pm/add_price', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }

  }

  public function act_add_price($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $this->t_model->setDoneCodefication($id_project);
      $this->t_model->setUpdateTimeProject($id_project);
      redirect('pm');
    } else {
      redirect('menu');
    }
  }

  public function detail_manage($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('pm/detail_manage_wo', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function edit_wo($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "PM") {
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('pm/edit_wo', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_wo($id_project)
  {
    if ($this->input->post('progress') == "Pending") {
      $this->t_model->editOrder($id_project);
      $query = $this->db->query("SELECT * FROM reason_project WHERE id_project = $id_project");
      if ($query->num_rows() > 0) {
        redirect('pm/manage_wo');
      } else {
        $this->add_reason_information($id_project);
      }
    } else if ($this->input->post('progress') == "Cancel") {
      $this->t_model->editOrder($id_project);
      $query = $this->db->query("SELECT * FROM reason_project WHERE id_project = $id_project");
      if ($query->num_rows() > 0) {
        redirect('pm/manage_wo');
      } else {
        $this->add_reason_information($id_project);
      }
    } else {
      $this->t_model->editOrder($id_project);
      redirect('pm/manage_wo');
    }
  }
}

// TODO: notif sama graphic nya
// TODO: export data dah intinya cari bae
 ?>
