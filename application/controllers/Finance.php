<?php
/**
 *
 */
class Finance extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    header('Cache-Control: no-cache, must-revalidate');
  }

  public function index()
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $this->dashboard();
    } else {
      redirect('menu');
    }
  }

  public function dashboard()
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $this->load->view('templates/header');
      $this->load->view('finance/dashboard');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function lending_tool()
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $data = array(
        'isi' => $this->t_model->getAddLending(),
        'isi_project' => $this->t_model->getListProject(),
        'tidakisi' => $this->t_model->getUnAddLending()
      );
      $this->load->view('templates/header');
      $this->load->view('finance/lending_tool',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_lending_tool($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $id_project = $this->uri->segment(3);
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('finance/add_lending_tool',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_lending($id_project)
  {
    $jos = $this->input->post('lending');
    $pinjaman = implode(", ", $jos);
    $this->t_model->addLending($id_project,$pinjaman);
    redirect('finance');
  }

  public function operational_funds()
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $data = array(
        'isi' => $this->t_model->getAddOps(),
        'jos' => $this->t_model->getUnAddOps()
       );
      $this->load->view('templates/header');
      $this->load->view('finance/operational_fund',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_ops($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $this->load->view('templates/header');
      $data = array(
        'id_project' => $id_project,
        'isi' => $this->t_model->getListWoDetail($id_project)
      );
      $this->load->view('finance/detail_ops_fund', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_ops_fund($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Finance") {
      $id_project = $this->uri->segment(3);
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('finance/add_ops_fund',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_ops($id_project)
  {
    $this->t_model->addOpsFund($id_project);
    redirect('finance');
  }

  // NOTE: tidak ada notif di finance

  // public function notification()
  // {
  //   $data['isi'] = $this->t_model->getListDetail();
  //   $this->load->view('finance/notification',$data);
  // }

  public function logout()
  {
    $this->session->unset_userdata('position');
    $this->session->unset_userdata('id_user');
    $this->index();
  }
  // TODO: fungsi graphic nya blm dibuat
}

 ?>
