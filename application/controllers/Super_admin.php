<?php
/**
 *
 */
class Super_admin extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    header('Cache-Control: no-cache, must-revalidate');
  }

  public function index()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $this->dashboard();
    } else {
      redirect('menu');
    }
  }

  public function dashboard()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $this->load->view('templates/header');
      $this->load->view('super_admin/dashboard');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function notification()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $query = $this->db->query("SELECT list_project.id_project, list_project.updated_at FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL ORDER BY list_project.updated_at ASC");
      $row = $query->row();
      if (isset($row)) {
        $data = array(
          'id_project' => $row->id_project,
          'tanggal'  => $row->updated_at
        );
      } else {
        $data = array(
          'id_project' => 'NULL',
          'tanggal' => 'NULL'
        );
      }
      $this->load->view('templates/header');
      $this->load->view('super_admin/notification',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_unfinished_report()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getListForNotif();
      $this->load->view('templates/header');
      $this->load->view('super_admin/list_unfinished_report',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_codefication()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getListUnfinishedCodefication();
      $this->load->view('templates/header');
      $this->load->view('super_admin/list_codefication',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_listcode($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getListWoDetail($id_project);
      $this->load->view('templates/header');
      $this->load->view('super_admin/detail_codefication',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function control_technician()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getTechnician();
      $this->load->view('templates/header');
      $this->load->view('super_admin/control_technician',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_tech()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $this->load->view('templates/header');
      $this->load->view('super_admin/add_tech');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_tech()
  {
    $this->t_model->setAddTech();
    redirect('super_admin/control_technician');
  }

  public function edit_tech($id_technician)
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data = array('id_technician' => $id_technician );
      $data['isi'] = $this->t_model->getDetailTechnician($id_technician);
      $this->load->view('templates/header');
      $this->load->view('super_admin/edit_tech', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_tech($id_technician)
  {
    $this->t_model->setEditTech($id_technician);
    redirect('super_admin/control_technician');
  }

  public function delete_tech($id_technician)
  {
    $this->t_model->setDeleteTech($id_technician);
    redirect('super_admin/control_technician');
  }

  public function control_user()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getListUser();
      $this->load->view('templates/header');
      $this->load->view('super_admin/control_user', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_user()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getPosition();
      $this->load->view('templates/header');
      $this->load->view('super_admin/add_user', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_user()
  {
    $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[18]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[18]');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[35]');
    if ($this->form_validation->run() == FALSE) {
      $this->add_user();
    } else {
      $password = $this->input->post('password');
      $enc_password = password_hash($password, PASSWORD_BCRYPT);
      $this->t_model->register($enc_password);
      redirect('super_admin/control_user');
    }
  }

  public function edit_user($id_user)
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data = array('id_user' => $id_user);
      $data['isi'] = $this->t_model->getDetailUser($id_user);
      $data['position'] = $this->t_model->getPosition();
      $this->load->view('templates/header');
      $this->load->view('super_admin/edit_user', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_user($id_user)
  {
    $password = $this->input->post('password');
    $enc_password = password_hash($password, PASSWORD_BCRYPT);
    $this->t_model->setEditUser($id_user ,$enc_password);
    redirect('super_admin/control_user');
  }

  public function delete_user($id_user)
  {
    $this->t_model->setDeleteUser($id_user);
    redirect('super_admin/control_user');
  }

  public function control_project()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isian'] = $this->t_model->getListProject();

      $query = $this->db->query("SELECT * FROM list_project WHERE technician != ' '");

      $config['total_rows'] = $query->num_rows();

      $config['base_url'] = base_url()."super_admin/control_project";

      $config['per_page'] = 5;

      $config['full_tag_open'] = "<ul class=\"pagination\">";
      $config['full_tag_close'] = "</ul>";

      $config['next_tag_open'] = "<li class=\"waves-effect\">";
      $config['prev_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_open'] = "<li class=\"waves-effect\">";
      $config['last_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_close'] = "</li>";
      $config['last_tag_close'] = "</li>";

      $config['num_tag_open'] = "<li class=\"waves-effect\">";
      $config['num_tag_close'] = "</li>";

      $config['next_tag_close'] = "</li>";
      $config['prev_tag_close'] = "</li>";

      $config['cur_tag_open'] = "<li class=\"disabled\">";
      $config['cur_tag_close'] = "</li>";

      $this->pagination->initialize($config);

      $this->load->view('templates/header');
      $this->load->view('super_admin/control_project',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_project($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $data['isian'] = $this->t_model->getListScheduleWithDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('super_admin/detail_project',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function tech_log()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $this->load->view('templates/header');
      $this->load->view('super_admin/tech_log');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function book_of_life()
  {
    $status = $this->session->userdata('position');
    if ($status == "Super Admin") {
      $this->load->view('templates/header');
      $this->load->view('super_admin/book_of_life');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function exp_data_bol()
  {

    $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');
    $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('not_input', 'Please Input Month and Year');
      redirect('super_admin/book_of_life');
    } else {
      $this->load->library('excel');

      $bulan = $this->input->post('bulan');
      $tahun = $this->input->post('tahun');

      $this->excel->setActiveSheetIndex(0);

      $this->excel->getActiveSheet()->setTitle('Book OF Life');

      $this->excel->getProperties()
                  ->setCreator("PT LUMBUNG RIANG COMMUNICATION")
                  ->setSubject("Book Of Life")
                  ->setTitle("Book Of Life")
                  ->setDescription("Book Of Life");
      // $this->db->select('*');
      // $this->db->from('list_project');
      // $this->db->join('detail_project','list_project.id_project = detail_project.id_project','left');
      // $this->db->join('confirmation_project', 'confirmation_project.id_project = detail_project.id_project', 'left');
      // $this->db->where('list_project.bulan', $bulan);
      // $this->db->where('list_project.tahun', $tahun);
      // $query = $this->db->get();

      $this->excel->getActiveSheet()
                  ->setCellValue('A1', 'BOOK OF LIFE')
                  ->setCellValue('A2', 'PT LUMBUNG RIANG COMMUNICATION')
                  ->setCellValue('A3', 'Periode'.' '.$bulan.' '.$tahun);

      $this->excel->getActiveSheet()
                  ->mergeCells('A1:T1')
                  ->mergeCells('A2:T2')
                  ->mergeCells('A3:T3');

      $this->excel->getActiveSheet()->getStyle('A1:A3')->getAlignment()->setHorizontal('center');
      $this->excel->getActiveSheet()->getStyle('A1:A3')->applyFromArray(
        array(
          'font' => array(
            'bold' => true,
            'size' => 14
          )
        )
      );

      $this->excel->getActiveSheet()
                  ->setCellValue('A5', 'NO')
                  ->setCellValue('B5', 'TANGGAL')
                  ->setCellValue('C5', 'PROJECT')
                  ->setCellValue('D5', 'CUSTOMER')
                  ->setCellValue('E5', 'IDWO')
                  ->setCellValue('F5', 'SO')
                  ->setCellValue('G5', 'PIC INDOSAT')
                  ->setCellValue('H5', 'LOCATION')
                  ->setCellValue('I5', 'TECHNICIAN')
                  ->setCellValue('J5', 'KETERANGAN')
                  ->setCellValue('K5', 'STATUS')
                  ->setCellValue('L5', 'CID')
                  ->setCellValue('M5', 'HARGA')
                  ->setCellValue('N5', 'NO BOQ')
                  ->setCellValue('O5', 'NO BA')
                  ->setCellValue('P5', 'NO PB')
                  ->setCellValue('Q5', 'NO PO')
                  ->setCellValue('R5', 'NO INVOICE')
                  ->setCellValue('S5', 'PAID')
                  ->setCellValue('T5', 'TGL INV');

      $this->excel->getActiveSheet()->getStyle('A5:T5')->applyFromArray(
        array(
          'font' => array(
            'bold' => TRUE
          ),
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        )
      );

      $baris = 6;

      $query = $this->db->query(
        " SELECT * FROM list_project
          LEFT JOIN detail_project
          ON list_project.id_project = detail_project.id_project
          LEFT JOIN confirmation_project
          ON confirmation_project.id_project = detail_project.id_project
          WHERE list_project.bulan = '$bulan' AND
          list_project.tahun = '$tahun'
        ");

      foreach ($query->result() as $row) {
        $this->excel->getActiveSheet()
                    ->setCellValue('A'.$baris, $row->id_project)
                    ->setCellValue('B'.$baris, $row->tanggal.' '.$row->bulan.' '.$row->tahun)
                    ->setCellValue('C'.$baris, $row->project)
                    ->setCellValue('D'.$baris, $row->customer)
                    ->setCellValue('E'.$baris, $row->id_wo)
                    ->setCellValue('F'.$baris, $row->so)
                    ->setCellValue('G'.$baris, $row->pic_indosat)
                    ->setCellValue('H'.$baris, $row->location)
                    ->setCellValue('I'.$baris, $row->technician)
                    ->setCellValue('J'.$baris, $row->information)
                    ->setCellValue('K'.$baris, $row->progress)
                    ->setCellValue('L'.$baris, $row->cid)
                    ->setCellValue('M'.$baris, $row->harga)
                    ->setCellValue('N'.$baris, $row->no_boq)
                    ->setCellValue('O'.$baris, $row->no_ba)
                    ->setCellValue('P'.$baris, $row->no_pb)
                    ->setCellValue('Q'.$baris, $row->no_po)
                    ->setCellValue('R'.$baris, $row->no_invoice)
                    ->setCellValue('S'.$baris, $row->status_paid)
                    ->setCellValue('T'.$baris, $row->tgl_invoice);
                    $baris++;
      }

      $filename = 'Book_Of_Life_Export_'.date("Y-m-d-H-i-s").'.xlsx';

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      header('Content-Disposition: attachment;filename="'.$filename.'"');

      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

      $objWriter->save('php://output');
    }
  }

  public function exp_tech_log()
  {

    $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');
    $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('not_input', 'Please input month and year');
      redirect('super_admin/tech_log');
    } else {
      $bulan = $this->input->post('bulan');
      $tahun = $this->input->post('tahun');
      $this->load->library('excel');

      $this->excel->setActiveSheetIndex(0);

      $this->excel->getProperties()
                  ->setCreator("PT LUMBUNG RIANG COMMUNICATION")
                  ->setSubject("Technician Log")
                  ->setTitle('Technician Log')
                  ->setDescription('Technician Log');

      $users = $this->t_model->getListScheduling();

      $this->excel->getActiveSheet()
                  ->setCellValue('A1', 'TECHNICIAN LOG')
                  ->setCellValue('A2', 'PT LUMBUNG RIANG COMMUNICATION')
                  ->setCellValue('A3', $bulan.' '.$tahun);

      $this->excel->getActiveSheet()
                  ->setCellValue('A5', 'NO')
                  ->setCellValue('B5', 'NO')
                  ->setCellValue('C5', 'TGL')
                  ->setCellValue('D5', 'PROJECT')
                  ->setCellValue('E5', 'CUSTOMER')
                  ->setCellValue('F5', 'IDWO')
                  ->setCellValue('G5', 'PIC INDOSAT')
                  ->setCellValue('H5', 'LOCATION')
                  ->setCellValue('I5', 'TECHNICIAN')
                  ->setCellValue('J5', 'KETERANGAN')
                  ->setCellValue('K5', 'STATUS');

      $this->excel->getActiveSheet()
                  ->mergeCells('A1:L1')
                  ->mergeCells('A2:L2')
                  ->mergeCells('A3:L3');

      $this->excel->getActiveSheet()->getStyle('A1:A3')->getAlignment()->setHorizontal('center');
      $this->excel->getActiveSheet()->getStyle('A1:A3')->applyFromArray(
        array(
          'font' => array(
            'bold' => true,
            'size' => 14
          )
        )
      );

      $this->excel->getActiveSheet()->getStyle('A5:K5')->applyFromArray(
        array(
          'font' => array(
            'bold' => true
          ),
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN,
              'color' => array(
                'argb' => 'FFFF0000'
              )
            )
          )
        )
      );

      $baris = 6;

      $query = $this->db->query("SELECT * FROM scheduling_project WHERE bulan = '$bulan' AND tahun = '$tahun'");

      foreach ($query->result() as $row) {
        $this->excel->getActiveSheet()
                    ->setCellValue('A'.$baris, $row->id_scheduling)
                    ->setCellValue('B'.$baris, $row->id_project)
                    ->setCellValue('C'.$baris, $row->tanggal)
                    ->setCellValue('D'.$baris, $row->project)
                    ->setCellValue('E'.$baris, $row->customer)
                    ->setCellValue('F'.$baris, $row->id_wo)
                    ->setCellValue('G'.$baris, $row->pic_indosat)
                    ->setCellValue('H'.$baris, $row->location)
                    ->setCellValue('I'.$baris, $row->technician)
                    ->setCellValue('J'.$baris, $row->keterangan)
                    ->setCellValue('K'.$baris, $row->status);
                    $baris++;
      }

      $this->excel->getActiveSheet()->getStyle('A6:K'.($baris-1))->applyFromArray(
        array(
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )
        )
      );

      $this->excel->getActiveSheet()->setTitle("Technician Log");

      $filename = 'Technician_Log_'.date("Y-m-d-H-i-s").'.xlsx';

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      header('Content-Disposition: attachment;filename="'.$filename.'"');

      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

      $objWriter->save('php://output');

    }

    // NOTE: yang diatas yang bener yang bawah salah
    //
    // $objPHPExcel = new PHPExcel();
    //
    // $objPHPExcel->getProperties()->setCreator("PT LUMBUNG RIANG COMMUNICATION");
    // $objPHPExcel->getProperties()->setLastModifiedBy("");
    // $objPHPExcel->getProperties()->setSubject("");
    // $objPHPExcel->getProperties()->setDescription("");
    //
    // $objPHPExcel->setActiveSheetIndex(0);
    //
    // $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO');
    // $objPHPExcel->getActiveSheet()->setCellValue('B1', 'NO');
    // $objPHPExcel->getActiveSheet()->setCellValue('C1', 'TGL');
    // $objPHPExcel->getActiveSheet()->setCellValue('D1', 'PROJECT');
    // $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CUSTOMER');
    // $objPHPExcel->getActiveSheet()->setCellValue('F1', 'IDWO');
    // $objPHPExcel->getActiveSheet()->setCellValue('G1', 'PIC INDOSAT');
    // $objPHPExcel->getActiveSheet()->setCellValue('H1', 'LOCATION');
    // $objPHPExcel->getActiveSheet()->setCellValue('I1', 'TECHNICIAN');
    // $objPHPExcel->getActiveSheet()->setCellValue('J1', 'KETERANGAN');
    // $objPHPExcel->getActiveSheet()->setCellValue('K1', 'STATUS');
    //
    // $filename = "Technician_Log_Export_Data_On".date("Y-m-d-H-i-s").'.xlsx';
    //
    // $objPHPExcel->getActiveSheet()->setTitle("Technician_Log");
    //
    // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    // header('Content-Disposition: attachment;filename="'.$filename.'"');
    // header('Cache-Control: max-age=0');
    //
    // $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel 2007');
    // $writer->save('php://output');
    // exit;
  }

  public function search_project()
  {

  }

  public function logout()
  {
    $this->session->unset_userdata('position');
    $this->session->unset_userdata('id_user');
    $this->index();
  }
}

// TODO: super admin bisa control semuanya
// TODO: bisa atur data userdata
 ?>
