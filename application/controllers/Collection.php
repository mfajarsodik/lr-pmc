<?php
/**
 *
 */
class Collection extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    header('Cache-Control: no-cache, must-revalidate');
  }

  public function index()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->dashboard();
    } else {
      redirect('menu');
    }
  }

  public function dashboard()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/dashboard');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function notification()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array(
        'ba' => $this->t_model->getNotInputBa(),
        'boq' => $this->t_model->getNotInputBoq(),
        'pb' => $this->t_model->getNotInputPb(),
        'po' => $this->t_model->getNotInputPo(),
        'no_invoice' => $this->t_model->getNotInputNoInvoice(),
        'paid' => $this->t_model->getNotInputPaid(),
        'tgl_invoice' => $this->t_model->getNotInputTglInv()
      );
      $this->load->view('templates/header');
      $this->load->view('collection/notification',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function control_project()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isian'] = $this->t_model->getListProject();

      $query = $this->db->query("SELECT * FROM list_project WHERE technician != ' '");

      $config['total_rows'] = $query->num_rows();

      $config['base_url'] = base_url()."collection/control_project";

      $config['per_page'] = 5;

      $config['full_tag_open'] = "<ul class=\"pagination\">";
      $config['full_tag_close'] = "</ul>";

      $config['next_tag_open'] = "<li class=\"waves-effect\">";
      $config['prev_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_open'] = "<li class=\"waves-effect\">";
      $config['last_tag_open'] = "<li class=\"waves-effect\">";

      $config['first_tag_close'] = "</li>";
      $config['last_tag_close'] = "</li>";

      $config['num_tag_open'] = "<li class=\"waves-effect\">";
      $config['num_tag_close'] = "</li>";

      $config['next_tag_close'] = "</li>";
      $config['prev_tag_close'] = "</li>";

      $config['cur_tag_open'] = "<li class=\"disabled\"><a class=\"red white-text\">";
      $config['cur_tag_close'] = "</a></li>";

      $this->pagination->initialize($config);

      $this->load->view('templates/header');
      $this->load->view('collection/control_project', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_project($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_project', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_onprogress()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/list_onprogress');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_done()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/list_done');
      $this->load->view('templates/footer');
    }
  }

  public function list_cancel()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/list_cancel');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_pending()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/list_pending');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_reason()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/list_reason');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function list_no_ba()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputBa();
      $this->load->view('templates/header');
      $this->load->view('collection/list_no_ba',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_no_ba($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_no_ba',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_ba($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array(
        'id_project' => $id_project
      );
      $this->load->view('templates/header');
      $this->load->view('collection/add_ba',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_ba($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->t_model->addNoBa($id_project);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function list_no_boq()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputBoq();
      $this->load->view('templates/header');
      $this->load->view('collection/list_no_boq',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_no_boq($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_no_boq',$data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_boq($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('collection/add_boq', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_boq($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->t_model->addNoBoq($id_project);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function list_no_pb()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputPb();
      $this->load->view('templates/header');
      $this->load->view('collection/list_no_pb', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_no_pb($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_no_pb', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_pb($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('collection/add_pb', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_pb($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->t_model->addNoPb($id_project);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function list_no_po()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputPo();
      $this->load->view('templates/header');
      $this->load->view('collection/list_no_po', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_no_po($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_no_po', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_po($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('collection/add_po', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_po($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->t_model->addNoPo($id_project);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function list_no_invoice()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputNoInvoice();
      $this->load->view('templates/header');
      $this->load->view('collection/list_no_invoice', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_no_invoice($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_no_invoice', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_no_invoice($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('collection/add_no_invoice', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_no_invoice($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->t_model->addNoInvoice($id_project);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function list_paid()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputPaid();
      $this->load->view('templates/header');
      $this->load->view('collection/list_paid', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_paid($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_paid', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_paid($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('collection/add_paid', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_paid($id_project)
  {
    if ($this->input->post('status_paid') == "Paid") {
      $this->t_model->addPaidStatus($id_project);
      redirect('collection');
    } elseif ($this->input->post('status_paid') == "Unpaid") {
      $this->t_model->addUnpaidStatus($id_project);
      redirect('collection');
    }
  }

  public function list_tgl()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getNotInputTglInv();
      $this->load->view('templates/header');
      $this->load->view('collection/list_tgl', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function detail_list_tgl($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getDetailProject($id_project);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_list_tgl', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_tgl($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data = array('id_project' => $id_project );
      $this->load->view('templates/header');
      $this->load->view('collection/add_tgl', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_tgl($id_project)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $satu = $this->input->post('tanggal');
      $dua = $this->input->post('bulan');
      $tiga = $this->input->post('tahun');
      $jos = $satu.' '.$dua.' '.$tiga;
      $this->t_model->addDateInvoice($id_project, $jos);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function book_of_life()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/book_of_life');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function export_rekap()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/exp_rekap');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function exp_rekap_ba()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      
    } else {
      redirect('menu');
    }
  }

  public function exp_data_bol()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');
      $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
      if ($this->form_validation->run() == FALSE) {
        $this->session->set_flashdata('not_input', 'Please Input Month and Year');
        redirect('super_admin/book_of_life');
      } else {
        $this->load->library('excel');

        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');

        $this->excel->setActiveSheetIndex(0);

        $this->excel->getActiveSheet()->setTitle('Book OF Life');

        $this->excel->getProperties()
                    ->setCreator("PT LUMBUNG RIANG COMMUNICATION")
                    ->setSubject("Book Of Life")
                    ->setTitle("Book Of Life")
                    ->setDescription("Book Of Life");

        $this->excel->getActiveSheet()
                    ->setCellValue('A1', 'BOOK OF LIFE')
                    ->setCellValue('A2', 'PT LUMBUNG RIANG COMMUNICATION')
                    ->setCellValue('A3', 'Periode'.' '.$bulan.' '.$tahun);

        $this->excel->getActiveSheet()
                    ->mergeCells('A1:T1')
                    ->mergeCells('A2:T2')
                    ->mergeCells('A3:T3');

        $this->excel->getActiveSheet()->getStyle('A1:A3')->getAlignment()->setHorizontal('center');

        $this->excel->getActiveSheet()->getStyle('A1:A3')->applyFromArray(
          array(
            'font' => array(
              'bold' => true,
              'size' => 14
            )
          )
        );

        $this->excel->getActiveSheet()
                    ->setCellValue('A5', 'NO')
                    ->setCellValue('B5', 'TANGGAL')
                    ->setCellValue('C5', 'PROJECT')
                    ->setCellValue('D5', 'CUSTOMER')
                    ->setCellValue('E5', 'IDWO')
                    ->setCellValue('F5', 'SO')
                    ->setCellValue('G5', 'PIC INDOSAT')
                    ->setCellValue('H5', 'LOCATION')
                    ->setCellValue('I5', 'TECHNICIAN')
                    ->setCellValue('J5', 'KETERANGAN')
                    ->setCellValue('K5', 'STATUS')
                    ->setCellValue('L5', 'CID')
                    ->setCellValue('M5', 'HARGA')
                    ->setCellValue('N5', 'NO BOQ')
                    ->setCellValue('O5', 'NO BA')
                    ->setCellValue('P5', 'NO PB')
                    ->setCellValue('Q5', 'NO PO')
                    ->setCellValue('R5', 'NO INVOICE')
                    ->setCellValue('S5', 'PAID')
                    ->setCellValue('T5', 'TGL INV');

        $this->excel->getActiveSheet()->getStyle('A5:T5')->applyFromArray(
        array(
         'font' => array(
         'bold' => TRUE
              ),
         'borders' => array(
         'allborders' => array(
         'style' => PHPExcel_Style_Border::BORDER_THIN
              )
            )
         )
       );

       $baris = 6;

       $query = $this->db->query(
                      " SELECT * FROM list_project
                        LEFT JOIN detail_project
                        ON list_project.id_project = detail_project.id_project
                        LEFT JOIN confirmation_project
                        ON confirmation_project.id_project = detail_project.id_project
                        WHERE list_project.bulan = '$bulan' AND
                        list_project.tahun = '$tahun'
                      ");

      foreach ($query->result() as $row) {
          $this->excel->getActiveSheet()
                      ->setCellValue('A'.$baris, $row->id_project)
                      ->setCellValue('B'.$baris, $row->tanggal.' '.$row->bulan.' '.$row->tahun)
                      ->setCellValue('C'.$baris, $row->project)
                      ->setCellValue('D'.$baris, $row->customer)
                      ->setCellValue('E'.$baris, $row->id_wo)
                      ->setCellValue('F'.$baris, $row->so)
                      ->setCellValue('G'.$baris, $row->pic_indosat)
                      ->setCellValue('H'.$baris, $row->location)
                      ->setCellValue('I'.$baris, $row->technician)
                      ->setCellValue('J'.$baris, $row->information)
                      ->setCellValue('K'.$baris, $row->progress)
                      ->setCellValue('L'.$baris, $row->cid)
                      ->setCellValue('M'.$baris, $row->harga)
                      ->setCellValue('N'.$baris, $row->no_boq)
                      ->setCellValue('O'.$baris, $row->no_ba)
                      ->setCellValue('P'.$baris, $row->no_pb)
                      ->setCellValue('Q'.$baris, $row->no_po)
                      ->setCellValue('R'.$baris, $row->no_invoice)
                      ->setCellValue('S'.$baris, $row->status_paid)
                      ->setCellValue('T'.$baris, $row->tgl_invoice);
                      $baris++;
                    }

      $filename = 'Book_Of_Life_Export_'.date("Y-m-d-H-i-s").'.xlsx';

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      header('Content-Disposition: attachment;filename="'.$filename.'"');

      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

      $objWriter->save('php://output');
      }
    } else {
      redirect ('menu');
    }
  }

  public function rekap_ba()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->getAllRekap();
      $this->load->view('templates/header');
      $this->load->view('collection/rekap_ba', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function add_new_rekap()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->load->view('templates/header');
      $this->load->view('collection/add_new_rekap');
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_add_rekap()
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      if ($this->input->post('payment_status') == 'Paid') {
        $final_status = 'Closed';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('invoice_no') != '') {
        $final_status = 'INVOICE NO';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('gr')) {
        $final_status = 'GR';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('po_date')) {
        $final_status = 'PO DATE';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('po_no')) {
        $final_status = 'PO NO';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('sc')) {
        $final_status = 'SC';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('pb_date')) {
        $final_status = 'PB DATE';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else if ($this->input->post('no_pb')) {
        $final_status = 'NO PB';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      } else {
        $final_status = '';
        $this->t_model->addNewRekap($final_status);
        redirect('collection');
      }
    } else {
      redirect('menu');
    }
  }

  public function detail_rekap($no_ba)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->detailRekap($no_ba);
      $this->load->view('templates/header');
      $this->load->view('collection/detail_rekap', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function edit_rekap($no_ba)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $data['isi'] = $this->t_model->detailRekap($no_ba);
      $this->load->view('templates/header');
      $this->load->view('collection/edit_rekap', $data);
      $this->load->view('templates/footer');
    } else {
      redirect('menu');
    }
  }

  public function act_edit_rekap($no_ba)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      if ($this->input->post('payment_status') == 'Paid') {
        $final_status = 'Closed';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('invoice_no') != '') {
        $final_status = 'INVOICE NO';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('gr')) {
        $final_status = 'GR';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('po_date')) {
        $final_status = 'PO DATE';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('po_no')) {
        $final_status = 'PO NO';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('sc')) {
        $final_status = 'SC';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('pb_date')) {
        $final_status = 'PB DATE';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else if ($this->input->post('no_pb')) {
        $final_status = 'NO PB';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      } else {
        $final_status = '';
        $this->t_model->editRekap($final_status, $no_ba);
        redirect('collection');
      }
    } else {
      redirect('menu');
    }
  }

  public function delete_rekap($no_ba)
  {
    $status = $this->session->userdata('position');
    if ($status == "Collection") {
      $this->t_model->deleteRekap($no_ba);
      redirect('collection');
    } else {
      redirect('menu');
    }
  }

  public function logout()
  {
    $this->session->unset_userdata('position');
    $this->session->unset_userdata('id_user');
    $this->index();
  }
// TODO: fungsi di collection blm semua
// TODO: buat export data ke excel sesuai yang di spreadsheet

}
 ?>
