<?php
/**
 * Developed by Desktop, Web and Mobile Apps Developer Team PT Lumbung Riang Communication
 */
class T_model extends CI_Model
{
  public function login($username)
  {
    $this->db->where('username', $username);
    $result = $this->db->get('user');
    if ($result->num_rows() == 1) {
      return $result->row(0)->id_user;
    } else {
      return false;
    }
  }

  public function register($enc_password)
  {
    $data = array(
      'username' => $this->input->post('username'),
      'password' => $enc_password,
      'name' => $this->input->post('name'),
      'position' => $this->input->post('position')
    );

    return $this->db->insert('user', $data);
  }

  public function getPosition() {
    $this->db->order_by('position_name');
    $query = $this->db->get('position');
    return $query->result_array();
  }

  public function getTechincianName()
  {
    $this->db->order_by('name_technician');
    $query = $this->db->get('technician_lr', '20', $this->uri->segment(3));
    return $query->result_array();
  }

  public function getUnAddLending()
  {
    $this->db->select('list_project.id_project, list_project.tanggal, list_project.bulan, list_project.tahun, list_project.pic_indosat, list_project.id_wo');
    $this->db->from('list_project');
    $this->db->limit($this->uri->segment(3));
    $this->db->join('detail_project', 'list_project.id_project = detail_project.id_project','left');
    $this->db->where('list_project.technician !=', ' ');
    $this->db->where('detail_project.peminjaman', NULL);
    $query = $this->db->get();
    // $query = $this->db->query("SELECT * FROM list_project LEFT JOIN detail_project ON list_project.id_project = detail_project.id_project WHERE list_project.technician != ' ' AND detail_project.peminjaman IS NULL");
    return $query->result_array();
  }

  public function getAddLending()
  {
    $this->db->select('*');
    $this->db->from('list_project');
    $this->db->limit('5', $this->uri->segment(3));
    $this->db->join('detail_project', 'list_project.id_project = detail_project.id_project', 'left');
    $this->db->where('list_project.technician !=', ' ');
    $this->db->where('detail_project.peminjaman !=', ' ');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getUnAddOps()
  {
    $query = $this->db->query(
      "SELECT list_project.id_project, list_project.id_wo,
       list_project.tanggal, list_project.bulan,
       list_project.tahun, list_project.pic_indosat
       FROM list_project
       LEFT JOIN detail_project
       ON list_project.id_project = detail_project.id_project
       WHERE list_project.technician != ' ' AND
       detail_project.dana_ops IS NULL
    ");
    return $query->result_array();
  }

  public function getAddOps()
  {
    $this->db->select('list_project.id_project, list_project.id_wo, list_project.tanggal, list_project.bulan, list_project.tahun, list_project.pic_indosat');
    $this->db->from('list_project');
    $this->db->join('detail_project', 'list_project.id_project = detail_project.id_project', 'left');
    $this->db->where('list_project.technician !=', ' ');
    $this->db->where('detail_project.dana_ops !=', ' ');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getTechnician()
  {
    $query = $this->db->get('technician_lr', '5', $this->uri->segment(3));
    return $query->result_array();
  }

  public function getDetailTechnician($id_technician)
  {
    $query = $this->db->query("SELECT * FROM technician_lr WHERE id_technician = $id_technician");
    return $query->result_array();
  }

  public function setEditTech($id_technician)
  {
    $data = array(
      'name_technician' => $this->input->post('name_technician'),
      'nik' => $this->input->post('nik'),
      'status' => $this->input->post('position')
    );
    $this->db->where('id_technician', $id_technician);
    return $this->db->update('technician_lr', $data);
  }

  public function setDeleteTech($id_technician)
  {
    $this->db->where('id_technician', $id_technician);
    return $this->db->delete('technician_lr');
  }

  public function setAddTech()
  {
    $data = array(
      'name_technician' => $this->input->post('name_technician'),
      'nik' => $this->input->post('nik'),
      'status' => $this->input->post('position')
   );
   return $this->db->insert('technician_lr', $data);
  }

  public function getListUser()
  {
    $query = $this->db->get('user');
    return $query->result_array();
  }

  public function getDetailUser($id_user)
  {
    $query = $this->db->where('id_user', $id_user);
    $query = $this->db->get('user');
    return $query->result_array();
  }

  public function setEditUser($id_user ,$enc_password)
  {
    $data = array(
      'username' => $this->input->post('username'),
      'password' => $enc_password,
      'name' => $this->input->post('name'),
      'position' => $this->input->post('position')
   );
   $this->db->where('id_user', $id_user);
   return $this->db->update('user',$data);
  }

  public function setDeleteUser($id_user)
  {
    $this->db->where('id_user', $id_user);
    return $this->db->delete('user');
  }

  public function checkIdWo($id_wo)
  {
    $this->db->where('id_wo', $id_wo);
    $query = $this->db->get('list_project');
    if ($query->num_rows() == 1) {
      return "DOUBLE";
    } else {
      return "BENAR";
    }
  }

  public function cekExistIdWoAndSo($id_wo, $so)
  {
    $this->db->where('id_wo', $id_wo);
    $this->db->where('so', $so);
    $query = $this->db->get('list_project');
    if ($query->num_rows() == 1) {
      return "DOUBLE";
    } else {
      return "BENAR";
    }
  }

  public function cekExistSo($so)
  {
    $this->db->where('so', $so);
    $query = $this->db->get('list_project');
    if ($query->num_rows() == 1) {
      return "DOUBLE";
    } else {
      return "BENAR";
    }
  }

  public function checkProjectIdForRandomWo()
  {
    $query = $this->db->query("SELECT COUNT(*) AS c FROM list_project");
    // return $query->result_array();
    return $query->row(0)->c;
  }

  public function addOrderWithRandIdWo($finalRandId)
  {
    $data = array(
      'tanggal' => $this->input->post('tanggal'),
      'bulan' => $this->input->post('bulan'),
      'tahun' => $this->input->post('tahun'),
      'project' => $this->input->post('project'),
      'customer' => $this->input->post('customer'),
      'id_wo' => $finalRandId,
      'so' => $this->input->post('so'),
      'pic_indosat' => $this->input->post('pic_indosat'),
      'location' => $this->input->post('location'),
      'cid' => $this->input->post('cid'),
      'created_at' => date("Y-m-d")
    );
    
    return $this->db->insert('list_project', $data);
  }

  public function addOrder()
  {
    $data = array(
      'tanggal' => $this->input->post('tanggal'),
      'bulan' => $this->input->post('bulan'),
      'tahun' => $this->input->post('tahun'),
      'project' => $this->input->post('project'),
      'customer' => $this->input->post('customer'),
      'id_wo' => $this->input->post('id_wo'),
      'so' => $this->input->post('so'),
      'pic_indosat' => $this->input->post('pic_indosat'),
      'location' => $this->input->post('location'),
      'cid' => $this->input->post('cid'),
      'created_at' => date("Y-m-d")
    );

    return $this->db->insert('list_project', $data);
  }

  public function getListWoNullTechnician()
  {
    $this->db->where('technician', '');
    $this->db->order_by('id_project', 'DESC');
    $query = $this->db->get('list_project', '5',$this->uri->segment(3));
    return $query->result_array();
  }

  public function getListWoDetail($id_project)
  {
    $this->db->where('id_project', $id_project);
    $query = $this->db->get('list_project');
    return $query->result_array();
  }

  public function scheduleOrderWo($id_project, $jos)
  {
    $data = array(
      'technician' => $jos,
      'tanggal' => $this->input->post('tanggal'),
      'bulan' => $this->input->post('bulan'),
      'tahun' => $this->input->post('tahun'),
      'updated_at' => date("Y-m-d")
    );

    $this->db->where('id_project', $id_project);
    return $this->db->update('list_project',$data);
  }

  public function addTechnicianForOrderWo($id_project, $jos)
  {
    $data = array(
      'technician' => $jos,
      'updated_at' => date("Y-m-d")
    );

    $this->db->where('id_project', $id_project);
    return $this->db->update('list_project',$data);
  }

  public function cekSchedulingWo()
  {
    $query = $this->db->query("SELECT DISTINCT list_project.id_project,
      list_project.id_wo, list_project.project, list_project.tanggal, list_project.bulan, list_project.tahun
      FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project ORDER BY list_project.id_project DESC");
    return $query->result_array();
  }

  // NOTE: ini buat liat satu example dari banyak data yang terschedule

  public function getListUnscheduledWo()
  {
    $query = $this->db->query("SELECT
      list_project.id_project, list_project.id_wo, list_project.project, list_project.tanggal,
      list_project.bulan, list_project.tahun, list_project.technician
      FROM list_project LEFT JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE scheduling_project.id_project IS NULL AND list_project.technician != ' '");
    return $query->result_array();
  }

  public function getListProject()
  {
    $this->db->where('technician !=', ' ');
    $this->db->order_by('id_project', 'DESC');
    $query = $this->db->get('list_project', '5',$this->uri->segment(3));
    return $query->result_array();
  }

  public function getReasonProject()
  {
    $this->db->select('*');
    $this->db->from('list_project','5',$this->uri->segment(3));
    $this->db->join('reason_project','list_project.id_project = reason_project.id_project','right');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getReasonProjectWhere($id_project)
  {
    $this->db->select('*');
    $this->db->from('list_project','5',$this->uri->segment(3));
    $this->db->join('reason_project','list_project.id_project = reason_project.id_project','right');
    $this->db->where('list_project.id_project',$id_project);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function setProgressWo($id_project)
  {
    $data = array(
      'progress' => $this->input->post('progress')
    );

    $this->db->where('id_project',$id_project);
    return $this->db->update('list_project',$data);
  }

  public function setReason($id_project)
  {
    $data = array('reason' => $this->input->post('reason') );
    $this->db->where('id_project', $id_project);
    return $this->db->update('reason_project', $data);
  }

  public function getLending()
  {
    $query = $this->db->query(
      "SELECT * FROM list_project
      JOIN detail_project ON list_project.id_project = detail_project.id_project");
    return $query->result_array();
  }

  public function addLending($id_project, $pinjaman)
  {
    $data = array(
      'id_project' => $id_project,
      'peminjaman' => $pinjaman,
      'detail_peminjaman' => $this->input->post('lending_detail')
    );
    return $this->db->insert('detail_project',$data);
  }

  public function getListDetail()
  {
    $query = $this->db->query(
      "SELECT * FROM detail_project JOIN list_project
      ON detail_project.id_project = list_project.id_project WHERE peminjaman != ' ' ");
    return $query->result_array();
  }

  public function addOpsFund($id_project)
  {
    $data = array(
      'dana_ops' => $this->input->post('ops'),
      'detail_dana_ops' => $this->input->post('ops_detail')
    );
    $query = $this->db->where('id_project' ,$id_project);
    return $this->db->update('detail_project', $data);
  }

  public function addScheduleProject($id_project, $jos)
  {
    $data = array(
      'id_project' => $id_project,
      'tanggal' => $this->input->post('tanggal'),
      'bulan' => $this->input->post('bulan'),
      'tahun' => $this->input->post('tahun'),
      'project' => $this->input->post('project'),
      'customer' => $this->input->post('customer'),
      'id_wo' => $this->input->post('id_wo'),
      'pic_indosat' => $this->input->post('pic_indosat'),
      'location' => $this->input->post('location'),
      'technician' => $jos,
      'keterangan' => $this->input->post('keterangan'),
      'status' => $this->input->post('progress')
    );

    return $this->db->insert('scheduling_project', $data);
  }

  public function editOrder($id_project)
  {
    $data = array(
      'tanggal' => $this->input->post('tanggal'),
      'bulan' => $this->input->post('bulan'),
      'tahun' => $this->input->post('tahun'),
      'project' => $this->input->post('project'),
      'customer' => $this->input->post('customer'),
      'id_wo' => $this->input->post('id_wo'),
      'so' => $this->input->post('so'),
      'pic_indosat' => $this->input->post('pic_indosat'),
      'location' => $this->input->post('location'),
      'information' => $this->input->post('information'),
      'progress' => $this->input->post('progress'),
      'cid' => $this->input->post('cid'),
    );
    $this->db->where('id_project', $id_project);
    return $this->db->update('list_project',$data);
  }

  public function addReason($id_project)
  {
    $data = array(
      'id_project' => $id_project,
      'reason' => $this->input->post('reason_information')
    );
    return $this->db->insert('reason_project', $data);
  }

  public function getListOnProgress()
  {
    $this->db->where('progress', 'On Progress');
    $this->db->where('technician !=', '');
    $query = $this->db->get('list_project', '5', $this->uri->segment(3));
    return $query->result_array();
  }

  public function getListCancel()
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE progress = 'Cancel' AND technician != ' ' ");
    return $query->result_array();
  }

  public function getListDone()
  {
    $this->db->where('progress', 'Done');
    $this->db->where('technician !=', ' ');
    $query = $this->db->get('list_project', '5', $this->uri->segment(3));
    return $query->result_array();
  }

  public function getListPending()
  {
    $this->db->where('progress', 'Pending');
    $this->db->where('technician !=', '');
    $query = $this->db->get('list_project', '5', $this->uri->segment(3));
    return $query->result_array();
  }

  public function getListReason($id_project)
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN reason_project on list_project.id_project = reason_project.id_project WHERE list_project.id_project = $id_project");
    return $query->result_array();
  }

  public function getDetailProject($id_project)
  {
    $query = $this->db->query(
      "SELECT list_project.id_project, list_project.tanggal, list_project.bulan,
      list_project.tahun, list_project.project, list_project.customer,
      list_project.id_wo, list_project.so, list_project.pic_indosat,
      list_project.location, list_project.technician, list_project.information,
      list_project.progress, list_project.cid, list_project.created_at,
      list_project.updated_at, list_project.finished_at,
      confirmation_project.report_teknis, confirmation_project.kodefikasi_harga,
      confirmation_project.harga, confirmation_project.no_boq,
      confirmation_project.no_ba, confirmation_project.nama_pekerjaan,
      confirmation_project.nominal_pekerjaan, confirmation_project.tanggal_submit_ba,
      confirmation_project.nama_pic, confirmation_project.revisi,
      confirmation_project.no_pb, confirmation_project.no_sc,
      confirmation_project.no_po, confirmation_project.no_gr,
      confirmation_project.no_invoice,
      confirmation_project.create_invoice, confirmation_project.status_paid,
      detail_project.peminjaman, detail_project.detail_peminjaman,
      detail_project.dana_ops, detail_project.detail_dana_ops
      FROM list_project LEFT JOIN confirmation_project
      ON list_project.id_project = confirmation_project.id_project
      LEFT JOIN detail_project ON detail_project.id_project = confirmation_project.id_project
      WHERE list_project.id_project = $id_project
      ");
    return $query->result_array();
  }

  public function getListForNotif()
  {
    $query = $this->db->query("SELECT list_project.id_project, list_project.id_wo, list_project.project, list_project.customer, list_project.tanggal, list_project.bulan, list_project.tahun FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
    return $query->result_array();
  }

  public function setReportTeknisDone($id_project)
  {
    $data = array(
      'id_project' => $id_project,
      'report_teknis' => 'Done',
    );
    return $this->db->insert('confirmation_project',$data);
  }

  public function getListScheduleWithDetailProject($id_project)
  {
    $query = $this->db->query("SELECT * FROM scheduling_project WHERE id_project = $id_project");
    return $query->result_array();
  }

  public function getListScheduling()
  {
    $query = $this->db->get('scheduling_project');
    return $query->result_array();
  }

  public function getListDetailSChedule($id_scheduling)
  {
    $query = $this->db->query("SELECT * FROM scheduling_project WHERE id_scheduling = $id_scheduling");
    return $query->result_array();
  }

  public function getFirstGraph()
  {
    $query = $this->db->query("SELECT COUNT(id_project) FROM `list_project` WHERE progress = 'Done' UNION SELECT COUNT(id_project) FROM `list_project` WHERE progress = 'On Progress' UNION SELECT COUNT(id_project) FROM `list_project` WHERE progress = 'Cancel' UNION SELECT COUNT(id_project) FROM `list_project` WHERE progress = 'Pending'");
    $row = $query->row(0);
    $row = $query->row(1);
    return $row;
  }

  public function getNotifReportTeknisPm()
  {
    $query = $this->db->query("SELECT * FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL");
    return $query->result_array();
  }

  public function getFilterDate($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3'");
    return $query->result_array();
  }

  public function getFilterDateLeader($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3' AND technician = ' '");
    return $query->result_array();
  }

  public function getFilterDateTechnicianNotNull($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE tanggal = '$isi1' AND bulan = '$isi2' AND tahun = '$isi3' AND technician != ' '");
    return $query->result_array();
  }

  public function getFilterDateScheduled($isi1, $isi2, $isi3)
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN scheduling_project
    ON list_project.id_project = scheduling_project.id_project
    WHERE list_project.technician != ' '
    AND scheduling_project.tanggal RLIKE '$isi1'
    AND scheduling_project.bulan RLIKE '$isi2'
    AND scheduling_project.tahun RLIKE '$isi3'
    ");
    return $query->result_array();
  }

  public function getListFilterProjectScheduled($jos)
  {
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.project RLIKE '$jos'
    ");
    return $query->result_array();
  }

  public function getFilterIdWoScheduled($jos)
  {
    $query = $this->db->query(
      " SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.id_wo RLIKE '$jos'
      ");
      return $query->result_array();
  }

  public function getFilterCustomerScheduled($jos)
  {
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.customer RLIKE '$jos'
    ");
    return $query->result_array();
  }

  public function getFilterWo($str)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE id_wo LIKE '$str' ");
    return $query->result_array();
  }

  public function getFilterPicScheduled($jos)
  {
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.pic_indosat RLIKE '$jos'
      ");
      return $query->result_array();
  }

  public function getFilterTechnicianScheduled($jos)
  {
    $query = $this->db->query(
      "SELECT * FROM list_project JOIN scheduling_project
      ON list_project.id_project = scheduling_project.id_project
      WHERE list_project.technician != ' '
      AND scheduling_project.technician RLIKE '$jos'
    ");
    return $query->result_array();
  }

  public function getFilterWoLeader($str)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE id_wo LIKE '$str' AND technician = ' '");
    return $query->result_array();
  }

  public function getFilterByProject($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos'");
    return $query->result_array();
  }

  public function getFilterByProjectTechnicianNotNull($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos' AND technician != ' '");
    return $query->result_array();
  }

  public function getFilterByProjectLeader($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE project RLIKE '$jos' AND technician = ' '");
    return $query->result_array();
  }

  public function getFilterByCustomer($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE customer RLIKE '$jos'");
    return $query->result_array();
  }

  public function getFilterByCustomerLeader($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE customer RLIKE '$jos' AND technician = ' '");
    return $query->result_array();
  }

  public function getFilterByCustomerTechnicianNotNull($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE customer RLIKE '$jos' AND technician != ' '");
    return $query->result_array();
  }

  public function getFilterBySo($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE so RLIKE '$jos'");
    return $query->result_array();
  }

  public function getFilterByLocation($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE location RLIKE '$jos'");
    return $query->result_array();
  }

  public function getFilterByPicIndosat($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE pic_indosat RLIKE '$jos'");
    return $query->result_array();
  }

  public function getFilterByCid($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE cid RLIKE '$jos'");
    return $query->result_array();
  }

  public function getFilterByTechnician($jos)
  {
    $query = $this->db->query("SELECT * FROM list_project WHERE technician RLIKE '$jos'");
    return $query->result_array();
  }

  public function getListUnfinishedCodefication()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.report_teknis = 'Done' AND confirmation_project.kodefikasi_harga = ' '");
    return $query->result_array();
  }

  public function setDoneCodefication($id_project)
  {
    $data = array(
      'kodefikasi_harga' => 'Done',
      'harga' => $this->input->post('price')
    );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project',$data);
  }

  public function setUpdateTimeProject($id_project)
  {
    $data = array('updated_at' => date("Y-m-d"));
    $this->db->where('id_project', $id_project);
    return $this->db->update('list_project', $data);
  }

  public function getNotInputBa()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.no_ba = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function getNotInputBoq()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.no_boq = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function getNotInputPb()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.no_pb = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function getNotInputPo()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.no_po = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function getNotInputNoInvoice()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.no_invoice = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function getNotInputPaid()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.status_paid = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function getNotInputTglInv()
  {
    $query = $this->db->query("SELECT * FROM list_project JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE confirmation_project.tgl_invoice = ' ' AND confirmation_project.kodefikasi_harga != ' '");
    return $query->result_array();
  }

  public function addNoBa($id_project)
  {
    $data = array(
      'no_ba' => $this->input->post('no_ba') );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project',$data);
  }

  public function addNoBoq($id_project)
  {
    $data = array('no_boq' => $this->input->post('no_boq') );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function addNoPb($id_project)
  {
    $data = array('no_pb' => $this->input->post('no_pb') );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function addNoPo($id_project)
  {
    $data = array('no_po' => $this->input->post('no_po') );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function addNoInvoice($id_project)
  {
    $data = array('no_invoice' => $this->input->post('no_invoice') );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function addPaidStatus($id_project)
  {
    $data = array('status_paid' => 'Paid' );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function addUnpaidStatus($id_project)
  {
    $data = array('status_paid' => 'Unpaid' );
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function addDateInvoice($id_project, $jos)
  {
    $data = array('tgl_invoice' => $jos);
    $this->db->where('id_project', $id_project);
    return $this->db->update('confirmation_project', $data);
  }

  public function getAllRekap()
  {
    $query = $this->db->get('rekap_ba');
    return $query->result_array();
  }

  public function addNewRekap($final_status)
  {
    $data = array(
      'no_ba' => $this->input->post('no_ba'),
      'deskripsi' => $this->input->post('deskripsi'),
      'pm' => $this->input->post('pm'),
      'nominal' => $this->input->post('nominal'),
      'verifikasi' => $this->input->post('verifikasi'),
      'no_pb' => $this->input->post('no_pb'),
      'pb_date' => $this->input->post('pb_date'),
      'sc' => $this->input->post('sc'),
      'po_no' => $this->input->post('po_no'),
      'po_date' => $this->input->post('po_date'),
      'gr' => $this->input->post('gr'),
      'invoice_no' => $this->input->post('invoice_no'),
      'payment_status' => $this->input->post('payment_status'),
      'payment_date' => $this->input->post('payment_date'),
      'final_status' => $final_status,
      'notes' => $this->input->post('notes')
    );

    return $this->db->insert('rekap_ba', $data);
  }

  public function editRekap($final_status, $no_ba)
  {
    $data = array(
      'deskripsi' => $this->input->post('deskripsi'),
      'pm' => $this->input->post('pm'),
      'nominal' => $this->input->post('nominal'),
      'verifikasi' => $this->input->post('verifikasi'),
      'no_pb' => $this->input->post('no_pb'),
      'pb_date' => $this->input->post('pb_date'),
      'sc' => $this->input->post('sc'),
      'po_no' => $this->input->post('po_no'),
      'po_date' => $this->input->post('po_date'),
      'gr' => $this->input->post('gr'),
      'invoice_no' => $this->input->post('invoice_no'),
      'payment_status' => $this->input->post('payment_status'),
      'payment_date' => $this->input->post('payment_date'),
      'final_status' => $final_status,
      'notes' => $this->input->post('notes')
    );
    $this->db->where('no_ba', $no_ba);
    return $this->db->update('rekap_ba', $data);
  }

  public function deleteRekap($no_ba)
  {
    $this->db->where('no_ba', $no_ba);
    return $this->db->delete('rekap_ba');
  }

  public function detailRekap($no_ba)
  {
    $query = $this->db->where('no_ba', $no_ba);
    $query = $this->db->get('rekap_ba');
    return $query->result_array();
  }

  public function getFilterNIK($str)
  {
    $query = $this->db->where('nik', $str);
    $query = $this->db->get('technician_lr');
    return $query->result_array();
  }

  public function getFilterNameTechnician($str)
  {
    $query = $this->db->where('name_technician RLIKE', $str);
    $query = $this->db->get('technician_lr');
    return $query->result_array();
  }

  public function getFilterStatusTechnician($str)
  {
    $query = $this->db->where('status RLIKE', $str);
    $query = $this->db->get('technician_lr');
    return $query->result_array();
  }

  public function getDoubleIdWoAndSo()
  {
    $this->db->select("id_project ,id_wo, so, project, tanggal, bulan, tahun");
    $this->db->from("list_project");
    $this->db->limit('5', $this->uri->segment(3));
    $this->db->where('id_wo OR so IS NOT NULL');
    $this->db->group_by("id_wo, so HAVING COUNT(*) > 1");
    $query = $this->db->get();
    // $this->db->count_all('> 1') ini ga bener
    // $query = $this->db->query("SELECT id_wo, so, project, technician, tanggal, bulan, tahun ,COUNT(*) c FROM list_project WHERE id_wo OR SO IS NOT NULL GROUP BY id_wo, so HAVING c > 1");
    return $query->result_array();
    // $query = $this->db->get('technician_lr', '5', $this->uri->segment(3));
  }

  public function getDoubleIdWoNull()
  {
    $query = $this->db->query("SELECT id_wo, so, project, tanggal, bulan, tahun FROM list_project WHERE id_wo IS NULL GROUP BY id_wo HAVING COUNT(*) > 1");
    return $query->result_array();
  }

  public function getDetailDoubleIdWo($id_wo)
  {
    $query = $this->db->query("SELECT * from list_project WHERE id_wo LIKE '$id_wo' ");
    return $query->result_array();
  }



  // $this->db->select('list_project.id_project, list_project.tanggal, list_project.bulan, list_project.tahun, list_project.pic_indosat, list_project.id_wo');
  //   $this->db->from('list_project');
  //   $this->db->limit($this->uri->segment(3));
  //   $this->db->join('detail_project', 'list_project.id_project = detail_project.id_project','left');
  //   $this->db->where('list_project.technician !=', ' ');
  //   $this->db->where('detail_project.peminjaman', NULL);
  //   $query = $this->db->get();



// SELECT list_project.id_project, list_project.updated_at FROM list_project LEFT JOIN confirmation_project ON list_project.id_project = confirmation_project.id_project WHERE list_project.progress = 'Done' AND confirmation_project.report_teknis IS NULL ORDER BY list_project.updated_at ASC
// NOTE: query untuk lihat list report teknis yang belum done dari tanggal paling lama
}

 ?>
